#Blinderman Construction

##Dependencies
1. Ruby
2. Bower

##Getting Started
1. gem install bundler (Check RVM for correct gemset / ruby)
2. Install ruby dependencies: `bundle install`
3. Install bower dependencies: `bower install`
4. Start Jekyll Server: `jekyll serve --watch`
