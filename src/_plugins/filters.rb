module Jekyll
  module OcuFilter
    def concat(input, array)
      unless array.respond_to?(:to_ary)
        raise ArgumentError.new("concat filter requires an array argument")
      end
      InputIterator.new(input).concat(array)
    end
  end
  class InputIterator
    include Enumerable

    def initialize(input)
      @input = if input.is_a?(Array)
        input.flatten
      elsif input.is_a?(Hash)
        [input]
      elsif input.is_a?(Enumerable)
        input
      else
        Array(input)
      end
    end

    def join(glue)
      to_a.join(glue)
    end

    def concat(args)
      to_a.concat(args)
    end

    def reverse
      reverse_each.to_a
    end

    def uniq(&block)
      to_a.uniq(&block)
    end

    def compact
      to_a.compact
    end

    def empty?
      @input.each { return false }
      true
    end

    def each
      @input.each do |e|
        yield(e.respond_to?(:to_liquid) ? e.to_liquid : e)
      end
    end
  end
end

Liquid::Template.register_filter(Jekyll::OcuFilter)