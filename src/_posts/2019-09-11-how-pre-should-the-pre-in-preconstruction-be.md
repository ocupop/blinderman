---
title: How ‘Pre’ should the ‘Pre’ in Preconstruction be?
image: /uploads/unrulyducks-linkedin.jpg
intro: Preconstruction is when you get all your ducks in a row.
date: 2019-09-11 09:00:00
categories:
  - Expertise
staff:
  - adam-kucharski
video_thumbnail:
video_url:
event:
exclude_from_homepage: false
---

**The sooner the better.**

The Preconstruction phase (Precon) is a collaborative and comprehensive process that allows an owner to make informed decisions to maximize the value of a project and manage cost and risk. Precon involves all the advance planning to identify constructability concerns, analyze cost impacts, and make the most efficient use of resources and funds. Blinderman Precon services usually include the following:

* Conceptual Design Review & Budgeting
* Project Scope Definition
* Construction Approach and Logistics
* Phasing and Sequencing
* Scheduling
* Value Engineering
* Detailed Cost Estimating
* Constructability Review
* Procurement Plan to Ensure Quality

**Blinderman develops smart solutions by taking a proactive approach to risk analysis.**&nbsp;These unique solutions lead to reducing the unknowns, providing the right information for capital planning, and offering alternatives analysis to determine the optimal approach. This iterative process always ensures the best outcome for our clients.

Dive into your next project with a trusted partner who has the Precon skill and smarts you need to get and keep all of those ducks in line.

![](/uploads/ducksinrows-linkedin.jpg){: width="4746" height="3869"}