---
title: Extensive Renovations to Palmer Elementary School
image: /uploads/palmer.jpg
intro:
date: 2018-07-09 08:00:00
categories:
  - Expertise
staff:
  - steven-blinderman
video_thumbnail:
video_url:
event:
exclude_from_homepage: false
---

On Sunday, July 8, 2018, [John M. Palmer Elementary School](https://www.palmerpride.org/){: target="_blank"}&nbsp;hosted a ceremony to announce the construction of a $20 million annex and a turf field, in addition to the $13 million in renovations to the school’s main building which Blinderman is currently executing. The ceremony was attended by [Mayor Rahm Emanuel](https://twitter.com/ChicagosMayor?ref_src=twsrc%5Egoogle%7Ctwcamp%5Eserp%7Ctwgr%5Eauthor){: target="_blank"}, [Alderwoman Margaret Laurino (39th Ward)](http://www.aldermanlaurino.com/contact_us){: target="_blank"}, Palmer principal, Jennifer Dixon, along with students and their families.

![](/uploads/cps-palmer1.png)

The ceremony acknowledged the academic achievements at Palmer, and also formally kicked off Blinderman's project - a complex renovation that includes extensive concrete repairs, roofing and masonry work.

For over 47 years, Blinderman Construction has worked together with [Chicago Public Schools](https://twitter.com/ChiPubSchools?ref_src=twsrc%5Egoogle%7Ctwcamp%5Eserp%7Ctwgr%5Eauthor){: target="_blank"} (CPS), and the [Public Building Commission of Chicago](https://twitter.com/PBCChi){: target="_blank"} (PBC), to improve the quality of Chicago educational facilities.