---
title: Smart Solutions
image: /uploads/smart-solutions.jpg
intro: >-
  Great leaders are smart, physicists are smart, professors are smart,
  code-breakers are smart…
date: 2017-08-10 08:30:00
categories:
  - Expertise
staff:
  - david-blinderman
video_thumbnail:
video_url:
exclude_from_homepage: false
---


They are all smart in their own ways whether it is in their ability to teach something that seems unlearnable or finding solutions for problems that seem unsolveable.

Contractors are not usually numbered among the "smart" professions. Quite the reverse, some people think contractors barely walk upright. I know what they mean – contractors can often be… well… rather one-dimensional.

After all, a construction project is not like sending a rocket to the moon. Though some projects can be very difficult, many are not. The challenge in construction is that every project is unique, with its own set of complications. Logistics, site constraints, weather, resources, personalities and time are only a few of the variables that make every project different.

Blinderman prides itself on our ability to deliver the <u>smart</u> solutions to these unique challenges, time and time again, each challenge different than the last. It’s not about having bigger brains or better grades in school. It's about having the right combination of experience, intellect, drive, passion and Singular Care that allows us to bring you the Smart Solution.

This competency we've developed over nearly five decades means we can get to the better solution, faster. Time is major component of any construction project. The faster something gets figured out, the faster work can proceed. The faster work can proceed, that faster your facility can operate. We are driven to find the best and smartest solutions because we care, deeply, about you, your project and your outcome.

At Blinderman we stand up straight. At the top of our upright posture, our noggins are filled with ingenuity, craft, and ability, all put to work every day for every client. So, we’ll leave the moonshots for others – we’re perfectly happy here on terra firma solving your very real construction challenges.