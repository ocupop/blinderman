---
title: Blinderman hosts inSTEM Mentor Luncheon
image: /uploads/instem-luncheon.jpg
intro:
date: 2018-06-28 08:00:00
categories:
  - Culture
  - Events
staff:
  - jasmin-mejia
  - marisa-milos
  - allison-lynch
  - natalie-house
  - janet-rodriguez
video_thumbnail:
video_url:
event: fiesta-on-fulton
exclude_from_homepage: true
---

Now in it's fifth year, The inSTEM Program has grown to offer year-round programs and a Mentor & Leadership component for high school girls and college women. On June 27, 2018 Blinderman hosted a luncheon for the inSTEM program, including 18 high school program mentors, staff, and executive board members.

Upon arrival, mentors were given a tour of Blinderman’s office and had the opportunity to connect with the Blinderman team and share their experiences within the AEC industry. During lunch, female Blinderman construction professionals gathered together to share their professional paths to success within the AEC industry.

inSTEM's stability and growth continues to be fueled by DePaul's College of Education's commitment to foundational resources and additional critical funding and support by [Motorola Solutions Foundation](https://www.motorolasolutions.com/en_us/about/company-overview/corporate-responsibility/motorola-solutions-foundation.html), [Chicago Department of Family & Support Services](https://www.cityofchicago.org/city/en/depts/fss.html) and Blinderman Construction.