---
title: How Blinderman Helps Bring Your Idea to Reality
image: /uploads/touch1a.jpg
intro: Where will your next great idea come from?
date: 2019-08-21 08:00:00
categories:
  - Expertise
staff:
  - adam-kucharski
video_thumbnail:
video_url:
event:
exclude_from_homepage: false
---

**Where will your next great idea come from?**<br>Companies typically start brainstorming sessions with a string of generic and only sometimes energetic directives. “Get creative\!”. “Think outside the box\!”. “There are no bad ideas\!” You’ve been there, racking your brain trying to come up with something great or different while burdened with anxiety over that growing to-do list looming over your head. Frustrating. Almost impossible.

**Great ideas take time.** Deferring judgment and evaluation is an important step. It’s about thinking big no matter how impossible that crazy idea might be to implement. Brainstorming is not the time to judge or evaluate but rather a time to get everything out in the open.

**As an individual struggling to develop creative solutions it might be helpful to change up your routine.** Go out to lunch with a new coworker, take a different route to work, ask for a new assignment or take it upon yourself to learn a new skill. Changing a habit can change the way you approach problems. In the end, the best ideas become reality because they solve an important problem.

**Developing an idea, however, is not enough.** Ideas come and go, but those who succeed take action.

**Blinderman can help.** It is with Singular Care that we start working with our clients very early in the process to help bring your idea to reality. In an industry fraught with complexity and risk, we believe our ability to collaborate, to dig in and analyze, to plan and engineer a construction solution that capitalizes on assets and mitigates risk offers a clear advantage to you.

**So, keep those ideas coming.**