---
title: An inSTEM Update from Blinderman
image: /uploads/fingers-crossed-campaign-blog-1.jpg
intro: >-
  Fingers crossed that we will be able to gather together later this year in
  support and celebration of the girls and women of inSTEM.
date: 2021-01-28 17:00:00
categories:
  - Culture
  - Announcements
staff:
  - jasmin-mejia
video_thumbnail:
video_url:
event:
exclude_from_homepage: false
---

We’ve missed you since our 2019 inSTEM fundraiser and wanted to provide an update on the program.&nbsp;

Despite the unpredictability of 2020, the inSTEM program did not miss a beat inspiring, encouraging, and mentoring girls toward STEM learning and careers. We understand that 2020 was a challenging year for everyone, and we overwhelmingly appreciate the industry partners and individuals that help us to continue with this important work. As our girls progress to become leaders of the future, our inSTEM program continues to rely on your encouragement and support.

We are cautiously optimistic about gathering in an appropriate manner this fall. Now, more than ever, the inSTEM program needs further encouragement and support as the girls progress to become leaders of the future.

**Read more below to learn about inSTEM's 2020 success and their planned 2021 initiatives.&nbsp;**

**Virtual Success**<br>Because of your generosity and the success of our 2019 Grand Bazaar fundraiser, coupled with inSTEM’s ability to pivot to a virtual setting, the program continued to support over **100 girls** **and women** **in STEM in 2020.**&nbsp;We are overwhelmingly appreciative of all our partners, sponsors and individual supporters\!

**University Bound**<br>The first class of inSTEM mentors are now seniors and will graduate high school this year. We are so proud of all of these young women as they navigate the challenging college application and admissions process. They are pursuing STEM career paths, studying Engineering, Computer Science, Chemistry, and Medicine.

We have extended an open invitation for them to continue their involvement with inSTEM as college mentors.

**Meaningful Partnerships**<br>We want to highlight and thank our partnership with the Chicago office of [Simpson Gumpertz & Heger](https://www.sgh.com/){: target="_blank" rel="noopener"}, an international engineering firm. For the past two summers, 13 women engineers have led the inSTEM participants though a hands-on Introduction to Civil Engineering experience and provided individual project-based engineer learning kits to each participant. &nbsp;&nbsp;

**Professional Mentorship**<br>A Professional Leadership & Mentorship program for new inSTEM college mentors is underway.

All STEM professional women are welcome to join as we continue to expand this impactful program\! Contact [Blinderman](mailto:info@blinderman.com?subject=Getting%20Involved%20with%20inSTEM){: target="_blank"} to learn more about participating.

**Teacher Development**<br>Another new inSTEM initiative this year is the inclusion of a Professional Development Program for Middle School Teachers. This pilot program is being offered through DePaul as an inSTEM micro-credential certificate to teachers in the NCAE. The goal of this program is to increase the STEM pipeline of educators and students.

**Get Involved**<br>Don’t hesitate to contact us at [info@blinderman.com&nbsp;](mailto:info@blinderman.com?subject=Getting%20Involved%20with%20inSTEM)with any thoughts on support or program questions. Let's keep your fingers crossed that we will be able to gather together personally in support and celebration of the girls and women of inSTEM.
