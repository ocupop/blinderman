---
title: Underscoring the Importance of Hiring Women
image: /uploads/women-in-construction-final.jpg
intro: "Here's a quick introduction to just some of the women who work at Blinderman."
date: 2017-03-06 10:00:00
categories:
  - Culture
staff:
  - jasmin-mejia
video_thumbnail:
video_url:
exclude_from_homepage: false
---


At Blinderman, we underscore the importance of hiring women, both in the field and within our office, and the impact women have on our vision, growth and purpose. Here's a quick introduction to just some of the women who work at Blinderman.

As a member of Blinderman’s leadership backbone, VP of Marketing & Communications, Christine Blevins, with almost 40 years of experience in design and construction – and over 10 of those years with Blinderman – is one of the authors of our company purpose, vision, value and strategic course.

Our Safety Director, Brittany Long, a Certified Journeywoman Carpenter is instrumental in developing and implementing our Safety program and instilling Safety as a VALUE throughout our company culture.

Margo Olson, AIA, our highly-esteemed Senior Project Manager, leads Blinderman’s recent plunge into the hospitality sector, including managing with great success the renovation of a boutique line of Graduate Hotels for AJ Capital Partners. Emma Ghariani, a former Blinderman intern, is now a full-time Project Manager is Emma is working on the Graduate Team with mentorship from Margo.

Superintendent/Safety Manager Sue Torres, a Certified Journeywoman Carpenter, consistently proves to be an asset to every project team and site she is assigned.

These are just a few of the women that makeup the Blinderman family, and we will continue to honor their hard work and remain committed to their success in the industry.