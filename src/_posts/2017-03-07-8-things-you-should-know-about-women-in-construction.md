---
title: 8 Things You Should Know About Women in Construction
image: /uploads/women-in-construction-2-final.jpg
intro: "In celebration of International Women's Day and Women in Construction Week, here are eight facts you should know about women in our industry:"
date: 2017-03-07 08:00:00
categories:
  - Culture
staff:
  - jasmin-mejia
video_thumbnail:
video_url:
exclude_from_homepage: false
---


1. The State of Illinois now officially recognizes the second week of March as Women in Construction Week, and it is being celebrated for the first time this week from March 5 – 11, 2017.
2. Gender diversity in construction is very low – truly a male dominated industry. Though women make up almost 50% of the U.S. workforce in general, women make up less than 10% of the construction industry workforce.
3. Though women on average in the U.S. earn 82.1% what men make, the gender pay gap is much narrower in the construction industry. In construction, women earn on average 93.4 percent what men make.
4. Women in construction management positions who are as well educated and as technically knowledgeable as their male counterparts are susceptible to negative stigmas based solely on the industry’s historic negative perception of women. These negative perceptions often deter women from seeking jobs in the construction industry.
5. Construction is an industry with a low concentration of female-owned businesses. Today, only 7% of all construction companies are owned by women.
6. There are safety and health issues specific to female construction workers. For example, many women in construction are assigned to work as flaggers – a job that has a high death rate. Poorly-fitting personal protective clothing and equipment designed for men and provided to women workers can compromise safety. These safety and health hazards in construction create barriers to women entering and remaining in this field. OSHA has a [webpage](https://www.osha.gov/doc/topics/women/) focusing on safety and health issues specific to women in construction.
7. National Association of Women in Construction (NAWIC) has 127 chapters across the country and 36 members in its local chapter. The local chapter raises awareness for women in construction by offering business and education opportunities, as well as networking and leadership activities.
8. Women in construction still face challenges. But, if construction markets grow as projected, the projected labor shortage in an industry with low female participation creates strong, steady, good paying life-long career opportunities for women in every facet, from preconstruction and construction management levels to the trades.