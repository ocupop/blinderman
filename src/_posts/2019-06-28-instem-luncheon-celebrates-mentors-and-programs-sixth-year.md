---
title: inSTEM Luncheon Celebrates Mentors and Program's Sixth Year
image: /uploads/2019-instem-luncheon.jpg
intro:
date: 2019-06-28 09:00:00
categories:
  - Events
  - Culture
staff:
  - jasmin-mejia
video_thumbnail:
video_url:
event: grand-bazaar
exclude_from_homepage: true
---

In celebration of inSTEM's sixth anniversary, Blinderman hosted its annual luncheon honoring the program's Mentor & Leadership team, including 19 young women from high schools across the Chicagoland area on June 27, 2019.

inSTEM, offered through [DePaul University's College of Education](https://education.depaul.edu/Pages/default.aspx){: target="_blank"}, offers free year-round programs for young girls, and a Mentor & Leadership component for high school girls and college women.&nbsp;

inSTEM’s stability and growth continues to be fueled by DePaul’s College of Education’s commitment to foundational resources and additional critical funding and support by&nbsp;[Motorola Solutions Foundation](https://www.motorolasolutions.com/en_us/about/company-overview/corporate-responsibility/motorola-solutions-foundation.html),&nbsp;[Chicago Department of Family & Support Services](https://www.cityofchicago.org/city/en/depts/fss.html)&nbsp;and Blinderman Construction.