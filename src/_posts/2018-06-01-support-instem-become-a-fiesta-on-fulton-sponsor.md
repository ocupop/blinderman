---
title: 'Support inSTEM: Become a Fiesta on Fulton Sponsor'
image: /uploads/mailchimpheader.png
intro:
date: 2018-06-01 08:00:00
categories:
  - Culture
staff:
  - jasmin-mejia
video_thumbnail:
video_url:
event:
exclude_from_homepage: false
---

**Join us on Thursday, November 8, 2018 for [Fiesta on Fulton](https://blinderman.com/social-responsibility){: target="_blank"}, our third annual fundraiser to benefit [inSTEM](http://www.instematdpu.com/){: target="_blank"}.** This valuable educational program inspires girls through a free summer camp focused on Science, Technology, Engineering, and Mathematics.

This year's fundraiser will be held at [Carnivale](https://www.carnivalechicago.com/) from 4:30 – 7:30 p.m. Event activities will also feature inSTEM students and mentors, their latest projects, and share highlights from their camp experience.

<u>Help Us Reach Our Goal</u><br>**Fiesta on Fulton [Sponsorship Opportunities](/2018_FiestaOnFulton_SponsorshipLevels.pdf){: target="_blank"} are available until** **September 7, 2018**. Contact our Fiesta on Fulton Coordinator, [Jasmin Mejia](mailto:jmejia@blinderman.com?subject=Fiesta%20on%20Fulton%20Sponsorship&amp;body=Name%3A%0ACompany%3A%0ASponsorship%20Level%20(Tango%2C%20Rumba%2C%20or%20Cha%20Cha)%3A) to secure your sponsorship.

<u>Raising Awareness and Raising Funds</u><br>Through its fundraisers, Blinderman has raised a collective $64,000 to support inSTEM, a cause that is dear to us. Continued support will allow the program to grow in reach and remain a no-cost opportunity for Chicago Public School students from underserved communities.

<u>About inSTEM</u><br>inSTEM (Inspiring STEM in Girls) is a program offered through DePaul University's College of Education that builds on the fundamentals of the Young People’s Project&nbsp; for mathematics literacy. The purpose of the inSTEM program is to provide middle school girls, primarily in under-served Chicago Public Schools, unique opportunities to engage in STEM activities. These experiences include real-world investigations, problem solving, and technology integrations, all designed to enhance students’ mathematics and science content knowledge and promote students’ interest and confidence in advanced education and STEM careers.

Now in it's fifth year, The inSTEM Program has grown to offer year-round programs and a Mentor & Leadership component for high school girls and college women.