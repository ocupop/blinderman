---
title: "Singular Care &mdash; it's in our DNA."
image: /uploads/singular-care.jpg
intro: "We can't can't help it. We can't help but care about each and every one of our customers."
date: 2017-06-09 08:00:00
categories:
  - Expertise
staff:
  - david-blinderman
video_thumbnail:
video_url:
exclude_from_homepage: false
---


Several years ago, at a meeting of construction executive peers, I shared a story about the lengths our company goes to please a particularly demanding public-sector customer when someone asked me, “Your low bid will get you the next job regardless. Why do you give a #%@ what this customer thinks of you?"

My answer was simple: We can't can't help it. We can't help but care about each and every one of our customers. We recognized long ago that providing care – Singular Care – is basic to our nature as people and as a company. It is literally in our DNA. This is how my and Steve's dad operated, and because it is how we continue to operate, we attract people to work with us who operate on the same wavelength, and Singular Care is ingrained in our culture.

Developing relationships with our customers is not just a necessary part of doing business. No matter the project size, or what the future business prospects may be, at every juncture we try to establish personal intimacy with the individuals we work with.

So yeah, here at Blinderman we are sometimes kind of nosey. As active listeners we pay attention to details, and we ask questions – Why did you make that decision? What pressures and constraints are you working under? We know that cultivating an understanding of you and your worldview creates a special relationship, a bond…an intimacy that only helps us to help you.

So don't be surprised if we remember your dog's name, or that your kid is graduating preschool, or how you take your coffee. It's because we care to know you, both professionally and personally.

I believe this is what sets Blinderman apart from the rest. Our value proposition begins with the words Singular Care. These words were chosen quite purposefully. It’s who we are, and what we offer to those with whom we work. And it’s why our customers, subcontractors and vendors continue to work with us.

At Blinderman Construction, it’s about more than just business.