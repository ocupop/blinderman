---
title: The Power of Sharing Experience
image: /uploads/sharingexperience-1.jpg
intro: >-
  I recently returned from our bi-annual board and shareholders' meeting for
  Raffles Insurance, the insurance captive Blinderman Construction joined in
  2011…
date: 2017-11-16 16:00:00
categories:
  - Expertise
staff:
  - steven-blinderman
video_thumbnail:
video_url:
exclude_from_homepage: false
---


Raffles was founded over 30 years ago upon the concepts of shared risk, safety-focused operations and long-term financial stability. Raffles is the only captive where new companies must be referred by existing members. This model for home growing is successful, and Raffles has $270M in annual premium and 370 members, half of which attended this most recent meeting.

I continue to be impressed not only by our captive’s stature in the marketplace, but by the focus on safety as a prerequisite for long-term success. As a group, our partners are thoroughly engaged in managing the risks associated with operating their businesses, whether in manufacturing, trucking, food and beverage or construction. They are driven by leading, rather than lagging indicators, because positive outcomes result from best of class behaviors. The professionals who run the captive are experts in their field and a tremendous resource in educating and guiding our members.

Our captive meets in interesting places and is a great opportunity to meet and engage with thoughtful business leaders who love their companies and their employees. I listen to their stories and how they have improved and benefitted from being owners of an insurance company. Many claim their decision to join Raffles was the single best business decision in their careers.

We all gain by these shared experiences. Sitting in on the Risk Control meeting, one of our members lamented that his employees had been properly trained, but did not always seem fully engaged in the tasks at hand. He asked if anyone had insight on this problem. I immediately recalled Blinderman's most recent quarterly safety meeting and shared my takeaways from our session on "Situational Awareness," where we acknowledged the need for mindfulness in our work, given the many distractions we face daily. This was an opportunity to share what I believe is a Blinderman "best practice."

I always leave the Raffles Board meetings with a renewed sense of progress as we continue to develop and refine our safety culture and risk management planning. I'm intent and focused on opportunities for improvement, and I look forward to sharing experiences regarding safety and business initiatives critical to continued growth and success.