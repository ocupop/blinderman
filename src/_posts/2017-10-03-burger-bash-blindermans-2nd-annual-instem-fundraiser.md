---
title: 'Burger Bash: Blinderman''s 2nd Annual inSTEM Fundraiser'
image: /uploads/burgerbash-weblog-1.jpg
intro:
date: 2017-10-03 13:10:00
categories:
  - Culture
  - Events
staff:
  - jasmin-mejia
video_thumbnail:
video_url:
event: fiesta-on-fulton
exclude_from_homepage: false
---

On Thursday, October 19, 2017 Blinderman Construction will host **Burger Bash**, our second annual fundraiser to benefit [inSTEM](http://www.instematdpu.com/).

Blinderman’s commitment to encouraging women in STEM fields is steadfast and unwavering. Our goal for this event is simple: promote a cause that is impactful to the future of the Architecture/Engineering/Construction (AEC) industry in Chicago by sharing delicious local fare with good company. We have partnered with notable Chicago restauranteurs to serve burgers from:

* [The Gage](https://www.thegagechicago.com/)
* [Kuma’s Corner West Loop](https://kumascorner.com/)
* [Publican Anker](https://www.publicananker.com/)
* [Bad Hunter](https://badhunter.com/)

This fundraiser will be held in our West Loop office from 4:30 – 8 p.m. Event activities will also feature inSTEM students, their latest projects, and share highlights from their camp experience.

**Help Us Reach Our Goal**<br>Burger Bash ticket purchases and donations can be made online**:**&nbsp;[www.greenvelope.com/event/BurgerBashTickets](https://www.greenvelope.com/event/BurgerBashTickets)

**Raising Awareness and Raising Funds**<br>In 2016, Blinderman’s first annual fundraiser in support of [inSTEM](http://www.instematdpu.com/) – *Food Truck Fest* – welcomed 225 guests and raised $17,000 for the program. Recognizing the importance of initiatives that help to pave a brighter future for women in STEM,&nbsp;**Blinderman is seeking to double its fundraising goal in 2017**. 100% of ticket proceeds from this event will be donated to inSTEM, directly supporting Chicago’s young girls by giving them the opportunity to participate in inSTEM’s 2018 camp experience.

**Inspiration Behind inSTEM**<br>Driven by the growing awareness of under-representation of women in science, technology, engineering and mathematics (STEM) related careers, three DePaul University professors came together to create a camp and mentorship program for young girls focused on learning experiences incorporating STEM elements. With the generous help from the Motorola Solutions Foundation Innovation Generation Grant, DePaul's College of Education first launched the inSTEM program in 2014.

DePaul University’s inSTEM program is designed to inspire and strengthen competence in science, technology, engineering, and mathematics among underserved, middle school girls through a summer camp and mentorship program. Students are recruited from low-income Chicago Public Schools to participate in camp activities that include robotics, programming, computer coding and introduction to game development logic for girls in 6<sup>th</sup>&nbsp;through 8<sup>th</sup>&nbsp;grade. A related mentor training workshop is offered for 9<sup>th</sup>&nbsp;graders to develop positive self-perception and enhance their collaborative and leadership skills .