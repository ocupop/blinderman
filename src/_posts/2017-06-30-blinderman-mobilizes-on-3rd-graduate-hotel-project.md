---
title: Blinderman Mobilizes on 3rd Graduate Hotel Project
image: /uploads/graduateminn-extrendering.png
intro:
date: 2017-06-30 11:00:00
categories:
staff:
video_thumbnail:
video_url:
exclude_from_homepage: false
---


Last week, Blinderman Construction mobilized on its third [Graduate Hotel](http://graduatehotels.com/) project with AJ Capital Partners, renovating the former Commons Hotel in Minneapolis. Our team recently completed renovations to both [Graduate Lincoln](http://graduatelincoln.com/) and [Graduate Ann Arbor](http://graduateannarbor.com/).

This eight-story hotel, located on Washington Avenue in the middle of the [University of Minnesota](https://twin-cities.umn.edu/) campus, includes 20,000 square feet of meeting and event space, a fitness center, and a restaurant and bar called [The Beacon Public House](http://www.beaconpublichouse.com/). All guest rooms, common areas, meeting and event spaces, and the hotel lobby will be renovated as part of the conversion. Our engineering solutions ensure that the guest experience will not be affected while the hotel remains open during renovations.

This project is scheduled for completion by winter 2017, and will join the collection of Graduate Hotels found in other university-anchored cities across the country, including in Michigan, California, Georgia, Virginia, Wisconsin, Mississippi, Arizona, and Nebraska.