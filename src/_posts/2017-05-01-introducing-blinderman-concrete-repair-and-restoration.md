---
title: Introducing Blinderman Concrete Repair and Restoration
image: /uploads/concrete-smoothing.jpg
intro:
date: 2017-05-01 10:00:00
categories:
  - Announcements
staff:
  - dave-byrd
video_thumbnail:
video_url:
exclude_from_homepage: false
---


For years, Blinderman Construction has utilized its own forces to self-perform concrete repair and renovation as a general contractor. As a result of this long-term success, we are excited to announce the formation of a new business unit focused on the concrete repair market in the Chicagoland area.

Some of the typical concrete problems we solve for our clients involve:

* Water leaks
* Excessive floor moisture
* Worn or Damaged Decorative Concrete
* Cracked, crumbling, flaking, and falling concrete
* Tripping/safety Hazards
* ADA compliance
* Rebar corrosion
* Worn or damaged traffic and pedestrian membrane

Blinderman Concrete’s expertise and capabilities include the following:

* Structural repairs
* Cementitious toppings
* Traffic/pedestrian membranes
* Crack repair & injection
* Moisture mitigation systems
* Small pavement and structural projects
* ADA access upgrades
* Decorative concrete repair & replacement
* Decorative epoxy resin flooring

![Tom Lathrop](/uploads/versions/lathrop-t-bw-200---x----200-200x---.jpg)
<br>
<br>With over 28 years of experience in the repair/restoration industry, Tom Lathrop manages all aspects of Blinderman Concrete, from business development, to writing proposals, to managing our crews on site. This in-house expertise offers tremendous value in determining the best value solution for your project. If your property or project has concrete issues that are highlighted above, please feel free to contact us to discuss. Tom and I look forward to working with you.

Dave Byrd
<br>Concrete Business Development
<br>312.982.2603

Tom Lathrop
<br>Concrete Division Leader
<br>312.982.2583