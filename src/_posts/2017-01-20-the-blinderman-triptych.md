---
title: The Blinderman Triptych
image: /uploads/entry.gif
intro:
date: 2017-01-20 12:00:00
categories:
  - Culture
staff:
  - christine-blevins
video_thumbnail: /uploads/triptych.gif
video_url: 'https://vimeo.com/220677745'
exclude_from_homepage: false
---


Quite often, visitors to our office stop to admire and ask about the series of three paintings – the triptych – on display in our reception area. The paintings are signed "Blevins" and though people often assume I am the artist, I quickly shift credit where credit is due. The talented artist who painted the triptych is Brian Blevins, who also happens to be my husband.

In 2015 Blinderman Construction relocated our main office to a loft space in the west loop. A group of Blinderman staff members volunteered to plan and design our office space. As the design progressed it was determined that we needed a strong graphic introduction to Blinderman that would be visible when visitors first step off the elevator and approach our space.

Though I've been employed here since 2006, Brian Blevins and I were first introduced to Blinderman in 1998, when our graphic design studio, Blevins Design, was contracted to design a new corporate identity system for Blinderman Construction. As the designer of the distinctive Blinderman identity, plus multiple ensuing marketing pieces, and also by virtue of being married to the VP of MarkComm, Brian has maintained both a professional and personal relationship with the company for all these the years. Because he has a deep understanding of the firm, Brian was commissioned to create a piece that visually expressed who we are and what we do.

After determining the necessary size of the work – large! – logistics determined the best format would be a a triptych of 3 canvases. Brian first designed and built a custom easel to accommodate painting all 3 large canvases at the same time, to ensure cohesive continuity. Brian began his creative process by conducting a photo tour of our jobsites and the resulting photography inspired the dynamic composition and color of the final work. Check out this video that documents the creation of the Blinderman Triptych.