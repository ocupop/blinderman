---
title: 'A Take on Singular Care: A Business Development Manager’s Perspective'
image: /uploads/02-a-take-on-singular-care.jpg
intro:
date: 2019-05-27 09:00:00
categories:
  - Expertise
staff:
  - adam-kucharski
video_thumbnail:
video_url:
event:
exclude_from_homepage: false
---

The foundation of Blinderman’s value is **Singular Care**. As a newer member of the Blinderman team I had a sense of what this meant but it wasn’t until I experienced it first-hand that I completely understood its true impact.

My first assignment here at Blinderman was a cool renovation of a property donated by [Sterling Bay](https://www.sterlingbay.com/){: target="_blank"} to the city of Chicago – an old brick and timber manufacturing building being converted into the new West Loop [Chicago Public Library](https://www.chipublib.org/){: target="_blank"} Branch. Designed by [SOM](https://www.som.com/){: target="_blank"}, this adaptive reuse project had all of the challenges you would normally expect – working through unexpected discovered conditions, juggling the priorities of multiple project stakeholders, many early mornings followed by late nights – and through it all I understood Singular Care.

Leadership, including our CEO and COO, were involved on a consistent basis. Whether that is a site safety walk, guidance in project status meetings, or following up with project stakeholders it was clear how important each project is no matter the size. My counterpart in the field went out of his way to ensure everything ran smoothly for the design team and library staff. Our customers weren’t always aware of what was happening in the background, but that didn’t matter – we cared about doing the right thing.

Singular Care is about being thoughtful, going beyond what is expected, and getting into the details in way no one else will. Through my experience it is clear we do what it takes to deliver a quality product on-time and on budget, but it’s not only about achieving those outcomes, it is about **how** they are achieved. Caring is our culture. It’s more than just words. It’s what you get when you choose us.

**Singular Care. Smart Solutions. Certain Success. Blinderman.**