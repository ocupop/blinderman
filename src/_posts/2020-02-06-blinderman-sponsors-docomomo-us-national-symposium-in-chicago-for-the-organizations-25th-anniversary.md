---
title: >-
  Blinderman sponsors Docomomo US National Symposium in Chicago for the
  organization’s 25th anniversary
image: /uploads/01---chicagosymposium.jpg
intro: Blinderman sponsors Docomomo US National Symposium in Chicago
date: 2020-02-06 12:00:00
categories:
  - Announcements
staff:
video_thumbnail:
video_url:
event:
exclude_from_homepage: false
---

Sponsoring the [Docomomo US](https://www.linkedin.com/company/docomomo-us/){: target="_blank"} 2020 National Symposium in Chicago this June was a no-brainer. Docomomo is a non-profit dedicated to the preservation of modern architecture, landscapes, and design.

![](/uploads/crownhall-1.jpg){: width="650" height="433"}

Our team recently completed renovation work at [Illinois Institute of Technology](https://www.linkedin.com/company/illinois-institute-of-technology/){: target="_self"}'s S.R. Crown Hall (pictured above), also known by Time Magazine as "one of the world's most influential, inspiring and astonishing structures". Designed by Ludwig Mies van der Rohe in 1956, it's one of the crowning examples that tie the Docomomo organization to Chicago, where it traces its roots. The Chicago Symposium will offer access to significant modern places not always open to the public, along with insight from recognized experts in their field. Join us this June for expert lectures, unique tours, and their 25th-anniversary celebration. [\#moderninthemiddle](https://www.linkedin.com/feed/hashtag/?highlightedUpdateUrns=urn%3Ali%3Aactivity%3A6631271488626532352&amp;keywords=%23moderninthemiddle&amp;originTrackingId=hlgbhj2hm4YhBOiwBYErzA%3D%3D){: target="_self"}

For more information, refer to the official press release from Docomomo below.

&nbsp;

**Contact:** Michele Racioppi<br>**E:** [michele.racioppi@docomomo-us.org](mailto:michele.racioppi@docomomo-us.org)<br>**P**\: (347) 688-2177

**Docomomo US brings National Symposium to Chicago, the Crossroads of Modern America, for the organization’s 25th anniversary**

*Conference registration, including 15 uniquely crafted guided tours, opens today.&nbsp;***Tickets are available at&nbsp;**[**docomomo-us.org/events/national-symposium**](http://www.docomomo-us.org/events/nationalsymposium)**.**

CHICAGO, IL — This June, [Docomomo US](https://www.docomomo-us.org/){: target="_blank"}, a nonprofit dedicated to the preservation of modern architecture, landscapes, and design, will hold its 2020 National Symposium in Chicago, Illinois, the city to which the organization traces its roots.

It was in 1995 that a group of volunteers gathered during the first Preserving the Recent Past Conference organized by the National Park Service to set in motion a Docomomo chapter in the United States. Twenty-five years later, Docomomo US will return to Chicago to host the Symposium, **Crossroads of Modern America**, bringing architects, scholars, and modernism enthusiasts to the Midwest from June 3-6. “I cannot think of a more appropriate city to celebrate our 25th anniversary than Chicago, the place where it all began,” said Liz Waytkus, Docomomo US executive director.

From the riches of Oak Park and Frank Lloyd Wright sites, to the highest concentration of Mies van der Rohe buildings in the world at Illinois Institute of Technology, to downtown skyscrapers and a complex variety of suburban and residential works, “there is no greater city in the world to appreciate modern architecture than Chicago,” according to Gunny Harboe, a founding Docomomo US Director and original participant at the organizing meeting 25 years ago.The Chicago Symposium will offer access to significant modern places not always open to the public, along with insight from recognized experts in their field. “What has been most unique about putting this Symposium together,” Waytkus said, “is the sheer volume of great modern architecture found in Chicago. There is not another city in America with the variety and depth of architecture that can be seen in Chicago. Our challenge was to curate what to showcase and find unique ways for guests to experience it all.”

The event will feature lectures and keynote presentations, over 15 guided tours, and receptions at historic venues. Attendees will visit some of the Chicagoland area’s most significant midcentury places such as the Farnsworth House and Marina City Towers, but also lesser-known sites from Chicago’s South Side with architectural historian Lee Bey, and Postmodern icons like the James R. Thompson Center, which is currently threatened with demolition.

The site of Chicago has always been a place a crossroads, both geographically and for its place in architecture, culture, and technological change. Throughout the modern movement, innovations have blossomed at these intersections. “From the birth of the skyscraper at the end of the 19th century through the full expression of the Miesian modernism, Chicago has been at the forefront of innovation and modern design,” explained Harboe.The 2020 National Symposium will consider this history, Chicago’s role in the forms and ideas of modernism in Mid-America, and analogous examples across the country.

Registration for the Symposium will begin on February 6, 2020. The event is organized in collaboration with Docomomo US/Chicago and will offer a limited number of discounted tickets for Illinois, Indiana and Wisconsin residents. Scholarship opportunities for students and rising professionals will also be available.

The National Symposium is produced in partnership with AIA Chicago, Elmhurst Art Museum, Farnsworth House, Frank Lloyd Wright Building Conservancy, IIT College of Architecture, Landmarks Illinois, Mies van der Rohe Society, Preservation Chicago, School of the Art Institute of Chicago, Schweikher House Preservation Trust, Society of Architectural Historians, and the University of Illinois at Chicago.

Tickets are available at [www.docomomo-us.org/events/national-symposium](http://www.docomomo-us.org/events/national-symposium){: target="_blank"}. For additional information, contact [info@docomomo-us.org](mailto:info@docomomo-us.org).

The 2020 National Symposium is made possible with the generous support of the following sponsors (full list is in creation):

**World’s Fair Columbian Exposition&nbsp;**<br>Mills + Schnoering Architects, Wiss, Janney, Elstner Associates, Harboe Architects, PC

**Chicago Great Fire&nbsp;**<br>Architectural Resources Group, Bauer Latoza Studio, Berglund Construction, Bernacki & Associates, Central Building & Preservation, Ghafari Associates, Krueck + Sexton, Ratio

**Fort Dearborn&nbsp;**<br>Avison Young, Bailey Edward, Barry Solar, CYLA Design Associates, Inc., Doyle & Associates, DSGN, F.H. Paschen, Klein & Hoffman, MacDonald & Mack Architects, Shepley Bulfinch, Wheeler Kearns Architects, Inc., Woodhouse Tinucci Architects

**Chicago River**<br>BlindermanHubbellHydro-Thermo-Power, Inc.MTH IndustriesNia ArchitectsSolomon Cordwell BuenzTGRWAThornton Thomasetti ArchitectsWilliam Marritt

\#\#\#

**About Docomomo US**<br>Docomomo US is dedicated to the preservation of modern architecture, landscape and design. Through advocacy, education and documentation, we provide leadership and knowledge by demonstrating the importance of modern design principles including the social context, technical merits, aesthetics and settings of these important pieces of American history. Founded in the United States in 1995, Docomomo US is a non-profit organization led by a national Board of Directors and staff that represents a union of regional chapters that share its members’ knowledge of and enthusiasm for modern architecture and design. The Docomomo US National Symposium is the primary event in the United States for those interested in the preservation of modern architecture.<br>[www.docomomo-us.org](http://www.docomomo-us.org){: target="_blank"}