---
title: Fueling Your Go or No-Go Process
image: /uploads/touch-2-due-diligence-website.jpg
intro: >-
  You need information to make that Go/No-Go decision, and doing your due
  diligence is all about mitigating risk.
date: 2019-08-29 14:00:00
categories:
  - Expertise
staff:
  - adam-kucharski
video_thumbnail:
video_url:
event:
exclude_from_homepage: false
---

The process has to be comprehensive to explore all options and identify critical deal killers to determine if your great idea is really feasible.

**Finding the right partners and consultants during the due diligence stage of a commercial real estate transaction is essential.** The process of mitigating risk and ensuring a successful long-term investment requires deep understanding and planning of the property’s operations, finances, legal concerns, environmental considerations, and building condition. A comprehensive construction estimate based on real-world market conditions at the due diligence phase is crucial to making an informed Go/No-Go decision. This is where Blinderman adds value.

We emphasize engaging our preconstruction team early in the design process to evaluate site conditions, examine existing structures and understand the vision for the property. Our deep relationships with experts in civil and structural engineering, environmental consultants, and specialty trades will help to provide the knowledge required to make that informed decision. **Our main goal is to avoid surprises, be proactive with solutions, and be most efficient with every dollar.**

A thorough construction estimate along with Smart Solutions for identified project risks is what you can expect from our team. In combination with the proper legal, jurisdictional, and environmental due diligence, you will have what you need to make an informed and intelligent investment decision.