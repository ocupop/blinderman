---
title: Success – it’s in the bag.
image: /uploads/inthebag-website.jpg
intro:
date: 2019-09-19 10:00:00
categories:
  - Expertise
staff:
  - adam-kucharski
video_thumbnail:
video_url:
event:
exclude_from_homepage: false
---

**Some people say that there are two rules for success: \#1 Never reveal everything you know.**

…funny, right?<br>But that’s not how we view success here at Blinderman. Revealing and sharing everything we know with our customers and project partners is how we achieve real success.

* We know the builtworld is filled with risks and unknowns, and in this environment, success can be elusive without the right partners and relationships in place. That is why we take time to cultivate and care for collaborative relationships that are built on trust and mutual respect.
* We know the unique nature of every project requires the insight and expertise to dig deep into the details and develop smart solutions that eliminate risk.
* We know that a great idea pursued with care and due diligence, followed through with thorough preconstruction planning and precision execution leads to certain success.

In our complex and fragmented industry, the equation for us is actually quite simple:&nbsp;**Singular Care + Smart Solutions = Certain Success**

![](/uploads/flutes-website.jpg){: width="750" height="500"}