---
title: Advancements in Preconstruction
image: /uploads/advancement-in-precon.jpg
intro: We are excited to announce...
date: 2017-08-07 11:04:00
categories:
  - Announcements
staff:
  - steven-blinderman
video_thumbnail:
video_url:
exclude_from_homepage: false
---


We are excited to announce that [**Dave Byrd**](/staff/dave-byrd), Vice President of Business Development for the past 2 years, will shift to become Blinderman's Vice President of Preconstruction. Dave’s full understanding of the Blinderman vision, his thoughtful and analytical approach to preconstruction, plus his technical expertise and business acumen make him the perfect choice for taking on this crucial role. Dave will lead and oversee Blinderman's Precon group for both private and public pursuits.

We are also thrilled to announce that Superintendent [**Dave Guseman**](/staff/dave-guseman) has left Operations to join the Precon group as Preconstruction Manager, Public Sector. In this new position Dave will manage the production of estimates for our public sector pursuits. Dave’s deep experience in Operations and working together with public agency customers such as the Capital Development Board, the Public Building Commission, Chicago Public Schools and Chicago Department of Aviation will be invaluable to his success in developing public work.

Our continued growth and evolution is dependent upon having a cohesive, strong and dynamic Preconstruction Group. Please join us in wishing both Dave Byrd and Dave Guseman much success in their new endeavors.