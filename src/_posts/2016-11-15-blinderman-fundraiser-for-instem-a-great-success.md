---
title: Blinderman Fundraiser for inSTEM a Great Success
image: /uploads/instem.jpg
intro:
date: 2016-11-15 08:00:00
categories:
  - Culture
staff:
  - jasmin-mejia
video_thumbnail:
video_url:
exclude_from_homepage: false
---


The inclusion of women in STEM fields begin with girls, and we believe it is the responsibility of corporate leaders to address this gap early on. In 2016, we hosted Food Truck Fest and raised over $15,500 in support of inSTEM, a free week-long summer camp at DePaul University for underpriviledged, minority girls interested in STEM. We fully intend to continue these efforts in 2017 in support of inSTEM and young girls in the Chicagoland area.

[inSTEM](https://href.li/?http://instematdpu.weebly.com/) is a program sponsored by DePaul University that is designed to inspire and strengthen competence in Science, Technology, Engineering, and Mathematics (STEM) among underprivileged, middle school girls through a free, one-week camp.

Food Truck Fest was designed to meet two goals: 1) Promote a cause we as an organization know is impactful to the future of our industry; 2) Bring together a group of talented people and feed them delicious food.

One-hundred percent of the ticket proceeds from our event were donated to inSTEM, directly supporting these young girls by giving them the opportunity to participate in inSTEM’s 2017 camp experience.

With 250 guests in attendance, Blinderman catered from five of Chicago’s favorite food trucks: The Slide Ride, Tamale Spaceship, Boo Coo Roux, Yum Dum, and Baby Cakes. Each truck presented some of their unique specialties including a variety of sliders and tamales, gumbo, kimcheesy rice balls, and bite-size chicken-and-waffles.

Blinderman is planning their second fundraiser for Fall 2017 – stay tuned for event details to be released this summer.