---
title: Blinderman Awarded ASA Chicago's Outstanding General Contractor of 2017 - 2018
image: /uploads/1a-asa-awardsblog-1.jpg
intro:
date: 2018-05-04 08:00:00
categories:
  - Announcements
staff:
  - steven-blinderman
video_thumbnail:
video_url:
---

![](blob:https://app.cloudcannon.com/56ecdaad-1a3f-4304-bd9f-242f1f9b42d3)Blinderman Construction was awarded [Association of Subcontractors & Affiliates (ASA) Chicago's](https://www.asachicago.org/){: target="_blank"} Outstanding General Contractor of 2017-2018. We are honored to receive this award and greatly appreciative of the hard work and the impact our subcontractors have on our projects.

> “The outstanding general contractor runs a smooth, highly efficient and well-coordinated job. The company is prompt and considerate in processing and paying subcontractors’ requisitions, making timely progress payments and expeditious final payments. The company is extremely well managed and administrative practices are effective and productive. The company is reasonable and cooperative with respect to change orders, back charges and in complying with the terms of the subcontract.  In all respects, the company is understanding, reasonable and fair with its subcontractors.” - Virginia McFarland, Executive Director, ASA Chicago

We understand that building and maintaining strong relationships with the subcontracting community is so important to the ultimate success of our projects. I would like to personally congratulate all of the awardees at ASA Chicago's 24th Annual Awards and GC Council Recognition Evening.