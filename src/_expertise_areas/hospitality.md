---
layout: expertise-area
title: Hospitality
subtitle:
description_html: '<p>Delivering unique construction solutions for unique properties, Blinderman is rapidly expanding its portfolio of projects in a market sector that depends on the high level of craftsmanship, partnership and integrity we are driven to provide.</p>'
image: /img/expertise/Phillips_01.17.17_005.jpg
---


