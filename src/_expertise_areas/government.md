---
layout: expertise-area
title: Government
subtitle:
description_html: "<p>For over 45 years Blinderman has provided best value to federal and local municipal agencies by knowing how to cross all the t's and dot all the i's with thorough preplanning, extensive knowledge of government processes, and detailed document management.</p>"
image: /img/expertise/GLNB_10.08_10.jpg
---


