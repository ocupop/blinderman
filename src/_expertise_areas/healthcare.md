---
layout: expertise-area
title: Healthcare
subtitle:
description_html: '<p>Our significant expertise delivering complex renovation in mission-critical settings is paramount when taking on the challenges inherent working within the exacting demands of operational and highly technical healthcare facilities</p>'
image: /img/expertise/UofChg_MRI001.jpg
---


