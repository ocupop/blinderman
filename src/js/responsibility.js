/*
 * Ocupop
 *
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see
 * http://www.gnu.org/licenses/agpl-3.0.html.
 */
(function($) {
    $.behaviors('#sponsor-event', animation);
    function animation(container) {
      container = $(container);
      //progress bar animations timing
      var waypoint = new Waypoint({
        element: document.getElementById('sponsor-event'),
        handler: function(direction) {
          container.addClass('active');
          var sponsor_progress_bar = $('#sponsor-event.active .progress-bar');

          var sponsor_progress = sponsor_progress_bar.data('width') + '%';

          sponsor_progress_bar.css('width', sponsor_progress);

        },
        offset: 350
      });
      var today = moment();
      var sponsor_start = $('.days-left-sponsor').data('start');
      var sponsor_end = $('.days-left-sponsor').data('end');
      var sponsor_start_formatted = moment(sponsor_start);
      var sponsor_end_formatted = moment(sponsor_end);

      //find how many days between today and end of sponsor period
      var duration_sponsor = sponsor_end_formatted.diff(today, 'days');
      window.console.log('duration_sponsor', duration_sponsor);
      //show days left value
      $('.days-left-sponsor').text(duration_sponsor);

      //calculate how many total days in sponsor period
      var total_days_sponsor = sponsor_end_formatted.diff(sponsor_start_formatted, 'days');
      window.console.log('total days sponsor', total_days_sponsor);

      //calculate percentage of days left 
      var percent_sponsor = (duration_sponsor / total_days_sponsor) * 100;
      window.console.log('percent', percent_sponsor);
      $('.percent-complete-sponsor').text(percent_sponsor);

      $('.sponsor-progress').data('width', percent_sponsor);
      
      //check to see if sponsorship is still available
      var check_sponsor_status = moment().isAfter(sponsor_end_formatted);
      window.console.log('status', check_sponsor_status);

      if(check_sponsor_status == true) {
        window.console.log('yep');
        $('#sponsor-event').addClass('hide');
      }
      
    }

  

})(jQuery);



