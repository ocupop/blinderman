/*
 * Ocupop
 *
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see
 * http://www.gnu.org/licenses/agpl-3.0.html.
 */
(function($) {
    $.behaviors('.blog-posts', blog_posts);
    function blog_posts(container) {
      container = $(container);

      //check for url parameter
      var params = window.location.search.substring(1);
      if (params) {
        window.console.log(params);
        var filter = '.' + params;
        window.console.log('filter ', filter);
        $('.blog-posts').mixItUp({
          load: {
            filter: filter
          }
        });
      }

      // open blog filters mobile
      $('#mobile-filters-trigger').on('click', function(){
        $(this).toggleClass('active');
        $('.blog-filters-mobile').toggleClass('active');
      });

      //close mobile filters menu after filter has been clicked
      container.on('mixEnd', function(){
        window.console.log('mixend');
        $('.blog-filters-mobile').removeClass('active');
      });
    }

})(jQuery);