/*
 * Ocupop
 *
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see
 * http://www.gnu.org/licenses/agpl-3.0.html.
 */
(function($) {
    $.behaviors('.animation', animation);
    function animation(container) {
      container = $(container);
      //our values animations timing
      var waypoint = new Waypoint({
        element: document.getElementById('animation-idea'),
        handler: function(direction) {
          $('#animation-idea').addClass('active');
        },
        offset: 500
      });
      var waypoint = new Waypoint({
        element: document.getElementById('animation-reality'),
        handler: function(direction) {
          $('#animation-reality').addClass('active');
        },
        offset: 500
      });
      var waypoint = new Waypoint({
        element: document.getElementById('animation-build'),
        handler: function(direction) {
          $('#animation-build').addClass('active');
        },
        offset: 500
      });
      var waypoint = new Waypoint({
        element: document.getElementById('animation-experience'),
        handler: function(direction) {
          $('#animation-experience').addClass('active');
        },
        offset: 500
      });
      var waypoint = new Waypoint({
        element: document.getElementById('animation-great-idea'),
        handler: function(direction) {
          $('#animation-great-idea').addClass('active');
        },
        offset: 500
      });
      // container.each(function() {
      //   var waypoint = new Waypoint({
      //     element: document.getElementsByClassName("animation"),
      //     handler: function(direction) {
      //       container.addClass('active');
      //     },
      //     offset: 500
      //   });
      //   var inview = new Waypoint.Inview({
      //     element: $(this),

      //     enter: function(direction) {
      //       //window.console.log('Enter triggered with direction ' + direction);
            
      //     },

      //     entered: function(direction) {
      //       //window.console.log('Entered triggered with direction ' + direction);
      //       $(this.element).addClass('active');
      //     },
      //     exit: function(direction) {
      //       //window.console.log('Exit triggered with direction ' + direction);
      //     },
      //     exited: function(direction) {
      //       //window.console.log('Exited triggered with direction ' + direction);
      //       //$(this.element).removeClass('active');
      //     }
      //   })
      // });
    }

})(jQuery);

