//main.js
$(function() {
  $('.blog-posts').mixItUp();
});

$(document).on('ready', function(){
  $('.venobox').venobox(); 
  setTimeout(function(){ $('.hero-caption').addClass('in-view'); }, 1000);
  setTimeout(function(){ $('.caption-bg').addClass('animated fadeIn'); }, 2000);
  //show thanks message if url variable exists
  var params = window.location.search.substring(1);
  if (params.indexOf('thanks') > -1 ) {
    $('#confirmation').show();
    setTimeout(function(){ 
      $('#confirmation').fadeOut('slow');
    }, 4000);
  }

  var careers_params = window.location.search.substring(1);
  if (careers_params.indexOf('resume') > -1 ) {
    $('#confirmation-careers').show();
    setTimeout(function(){ 
      $('#confirmation-careers').fadeOut('slow');
    }, 4000);
  }

  var params = window.location.search.substring(1);
  if (params.indexOf('nav_false') > -1 ) {
    $('#project-nav').hide();
  }

  $('.megaMenu').on('show.bs.collapse', function(){
    $('.megaMenu.collapse.in').collapse('hide');
  });

  //megamenu
  $('#mainnav li[data-target]').on('click', function(){
    window.console.log($(this).data('target'));
    var menu = $(this).data('target');
    if($(menu).hasClass('active')){
      $(menu).removeClass('active');
      return
    }else{
      $('.megaMenu').removeClass('active');
      $(menu).addClass('active');
    }
  });

  // open search
  $('.search-trigger').on('click', function(){
    $('#search').addClass('active');
  });

  // close search
  $('.close-search').on('click', function(){
    $('#search').removeClass('active');
  });

  // open mobilenav
  $('.navbar-toggle').on('click', function(){
    $('#mobilenav').addClass('active');
  });

  // close mobilenav
  $('#mobilenav .icon-cross').on('click', function(){
    $('#mobilenav').removeClass('active');
  });

  //trigger for subcontractor drawer
  // $('#subcontractors').on('click', function(){
  //   $('#topnav-drawer').toggleClass('active');
  // });

  //slick carousel
  $('.slick-slider').slick({
    infinite: true,
    slidesToShow: 3,
    slidesToScroll: 3,
    responsive: [
        {
          breakpoint: 991,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            infinite: true
          }
        }
      ]  
  });
    
});

//footer images
function random_image(){
  var image = new Array ();
  image[0] = "/img/footer/01.jpg";
  image[1] = "/img/footer/02.jpg";
  image[2] = "/img/footer/03.jpg";
  image[3] = "/img/footer/04.jpg";
  image[4] = "/img/footer/05.jpg";
  image[5] = "/img/footer/06.jpg";
  image[6] = "/img/footer/07.jpg";
  image[7] = "/img/footer/08.jpg";
  image[8] = "/img/footer/09.jpg";
  image[9] = "/img/footer/10.jpg";
  image[10] = "/img/footer/11.jpg";
  image[11] = "/img/footer/12.jpg";
  image[12] = "/img/footer/13.jpg";
  image[13] = "/img/footer/14.jpg";
  image[14] = "/img/footer/15.jpg";
  var size = image.length
  var x = Math.floor(size*Math.random())
  $('#page-footer').css('background-image', 'url(' + image[x] + ')');
}

window.onload = function(){
  //window.console.log('fire random image');
  random_image();
}

