/*
 * Ocupop
 *
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see
 * http://www.gnu.org/licenses/agpl-3.0.html.
 */
(function($) {
  $.behaviors("#buy-tickets", animation);
  function animation(container) {
    container = $(container);
    //progress bar animations timing
    var waypoint = new Waypoint({
      element: document.getElementById("buy-tickets"),
      handler: function(direction) {
        container.addClass("active");
        var ticket_progress_bar = $("#buy-tickets.active .progress-bar");

        var ticket_progress = ticket_progress_bar.data("width") + "%";

        ticket_progress_bar.css("width", ticket_progress);
      },
      offset: 350
    });

    //ticket countdown logic

    var today = moment();
    var ticket_start = $(".days-left-tickets").data("start");
    var ticket_end = $(".days-left-tickets").data("end");
    var ticket_start_formatted = moment(ticket_start);
    var ticket_end_formatted = moment(ticket_end);
    window.console.log("days left Jeff tickets", ticket_end_formatted);
    var sponsor_end = $(".days-left-sponsor").data("end");
    var sponsor_end_formatted = moment(sponsor_end);

    //find how many days between today and end of ticket period
    var ticket_duration = ticket_end_formatted.diff(today, "days");
    window.console.log("ticket_duration", ticket_duration);
    //show days left value
    $(".days-left-tickets").text(ticket_duration);

    //calculate how many total days in ticket period
    var total_days_tickets = ticket_end_formatted.diff(
      ticket_start_formatted,
      "days"
    );
    window.console.log("total days tickets", total_days_tickets);

    //calculate percentage of days left
    var percent_ticket = (ticket_duration / total_days_tickets) * 100;
    window.console.log("percent ticket", percent_ticket);
    $(".percent-complete-ticket").text(percent_ticket);

    $(".ticket-progress").data("width", percent_ticket);

    //check to see if sponsorship is still available
    var check_ticket_status = moment().isAfter(ticket_start_formatted);
    window.console.log("status", check_ticket_status);

    if (check_ticket_status == true) {
      window.console.log("show tickets");
      $("#buy-tickets").removeClass("hide");
      $("#ticket-link").removeClass("hide");
    }

    //check to see if ticket sales are over
    var check_ticket_over_status = moment().isBefore(ticket_end_formatted);
    window.console.log("status", check_ticket_over_status);

    if (check_ticket_over_status == true) {
      window.console.log("done tickets");
      $("#buy-tickets").removeClass("hide");
      $("#ticket-link").removeClass("hide");
    }
  }
})(jQuery);
