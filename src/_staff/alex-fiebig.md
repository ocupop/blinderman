---
layout: bio
name: Alex Fiebig
job_title: Senior Project Manager
professional_image: /uploads/fiebiga-bw2020-small-2.jpg
personal_image: /uploads/fiebig-family-small.jpg
professional_affiliations:
  - OSHA 30-Hour
  - AED/CPR/First Aid
education:
  - achievement:
    institution:
staff_category:
  - operations
things_you_should_know:
  - '"When I''m not at work, I have five kids... I''m always working."'
  - If Alex could travel anywhere in the world, he would go to Australia.
  - If you name any movie, Alex probably hasn't seen it.
---

Alex Fiebig has 14 years of experience in the construction industry, with 11 years specific to concrete repair and restoration. As the Senior Project Manager for Blinderman's Concrete Division, Alex is responsible for leadership of the project team including overall onsite contract administration, subcontractor management, quality assurance, and correspondence with owners, architects and engineers.
