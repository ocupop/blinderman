---
layout: bio
name: Randy Thompson
job_title: Superintendent
professional_image: /uploads/thompsonr-bw-small.jpg
personal_image: /uploads/thompson-randy.jpg
professional_affiliations:
  -
education:
  - achievement: Certified Journeyman Carpenter
    institution:
  - achievement: 'EPA Lead Renovator for Lead Safety in Remodeling, Repair and Painting'
    institution:
  - achievement: OSHA 30-hour
    institution:
  - achievement: AED/CPR/First Aid
    institution:
staff_category:
  - operations
things_you_should_know:
  - >-
    When Randy was a kid, he wanted to grow up to be a carpenter – dream
    realized.
  - >-
    Randy collects vintage tools, his most prized possessions being the tools he
    inherited from his grandfather.
---


With 17 years of experience in construction, it has always been Randy’s personal goal to do the best he can to get the job done on-time and under budget. On site Randy is responsible for safety management, coordinating schedules with subcontractors and vendors, and directing self-performed work.