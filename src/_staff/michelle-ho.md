---
layout: bio
name: Michelle Ho
job_title: Project Engineer
professional_image: /uploads/hom-bw-small.jpg
personal_image:
professional_affiliations:
  -
education:
  - achievement: 'B.S., Civil Engineering'
    institution: Illinois Institute of Technology
  - achievement: OSHA 30-hour
    institution:
  - achievement: First Aid/CPR/AED
    institution:
staff_category:
  - operations
things_you_should_know:
  -
---

A recent graduate of IIT, Michelle Ho is a full-time Project Engineer responsible for providing administrative support, and on-site assistance for Project Managers. Michelle began her journey as part of our summer intern program. Michelle is also member of Blinderman's Diversity, Equity, and Inclusion (DEI) committee.
