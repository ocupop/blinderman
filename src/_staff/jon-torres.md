---
layout: bio
name: Jonathan Torres
job_title: Project Engineer
professional_image: /uploads/torresj2-bw-small.jpg
personal_image: /uploads/torresj-website.jpg
professional_affiliations:
  -
education:
  - achievement: B.A., Organizational Management
    institution: Concordia University Chicago
  - achievement: OSHA 30-hour
    institution:
  - achievement: AED/CPR/First Aid
    institution:
staff_category:
  - operations
things_you_should_know:
  - When Jonathan was a kid, he wanted to be a paleontologist when he grew up.
  - In his spare time, Jonathan collects vinyl records.
  - >-
    Being the first person in his family to receive a Bachelor's Degree is among
    Jonathan's proudest achievements.
---

Jonathan is a full-time Project Engineer responsible for providing administrative support, and on-site assistance for Project Managers.
