---
layout: bio
name: Noah LeCavalier
job_title: Internship Participant
professional_image: /uploads/nlecavalier-bw-small.png
personal_image: /uploads/nlecavalier-color.png
professional_affiliations:
  -
education:
  - achievement: B.S., Systems Engineering and Design (expected 2023)
    institution: University of Illinois at Urbana-Champaign
staff_category:
  - operations
things_you_should_know:
  - >-
    One of Noah's proudest achievements is his first time individually reverse
    engineering, modeling, and assembling a product: a toy helicopter.
  - When he's not at work, Noah can be found playing pickup sports.
  - >-
    If Noah could travel anywhere in the world, he would go to Santorini,
    Greece.
---

As a Blinderman Intern, Noah's work spans a variety of unique assignments. He primarily works within our Concrete Division. Outside of the Concrete Division, Noah spends time rotating between other company divisions, allowing exposure to a variety of opportunities within the commercial construction industry.
