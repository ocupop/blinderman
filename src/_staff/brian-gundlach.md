---
layout: bio
name: Brian Gundlach
job_title: Vice President, Regional Program
professional_image: /uploads/gundlach-b-bw.jpg
personal_image: /uploads/gundlachb.jpg
professional_affiliations:
  -
education:
  - achievement: B.S., Business Administration
    institution: Ohio State University
  - achievement: OSHA 30-hour
    institution:
  - achievement: AED/CPR/First Aid
    institution:
staff_category:
  - leadership
things_you_should_know:
  - >-
    Brian’s 6 months hiking the Appalachian Trial was one of the most
    influential experiences of his life.
  - >-
    Brian spent his formative years working for the Gundlach family HVAC
    installation and repair business in  Sandusky, OH.
  - >-
    The best career advice Brian ever received is: Never pass up the opportunity
    to be involved with something new or different.
---

Brian ensures the success of our projects and our people by forging interconnected relationships within all project teams, paying attention to the details, and finding smart solutions to challenges that arise. Brian believes in the concept of lifelong learning, and his focus on continued professional development results in a staff who always strive to ensure our projects are built to the highest quality standards. Application of this philosophy not only leads to satisfied customers, but to satisfying careers. Brian’s keen operational insight and know-how is critical to his contribution as a member of Blinderman’s Leadership and Strategic Planning teams.
