---
layout: bio
name: Rob Roth
job_title: Superintendent
professional_image: /uploads/roth-r-bw-small-1.jpg
personal_image: /uploads/rothr-bio.jpg
professional_affiliations:
  -
education:
  - achievement: 'B.S., Construction Management '
    institution: 'Bradley University '
  - achievement: LEED AP
    institution:
  - achievement: OSHA 30-hour
    institution:
  - achievement: AED/CPR/First Aid
    institution:
staff_category:
  - operations
things_you_should_know:
  - His name is  Rob, only the Government and his twin bother call him Robert.
  - >-
    Best career advice Rob has received: "It is a privilege to do what you chose
    to do, not what you have to do."
  - Rob can easily binge eat Cheetos Puffs.
---

With 15 years of experience, it's Rob's responsibility as a superintendent to be in the field pushing a job forward, ensuring it is done right, on-time, and safely. Rob accomplishes this by working closely with his team and looking ahead to anticipate obstacles, and solve problems.
