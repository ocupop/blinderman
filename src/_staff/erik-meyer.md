---
layout: bio
name: Erik Meyer
job_title: Senior Estimator
professional_image: /meyer-e-bw2-small.jpg
personal_image: /meyer-2021.jpg
professional_affiliations:
  - OSHA 30
education:
  - achievement:
    institution:
  - achievement: LEED AP
    institution:
  - achievement: OSHA 30
    institution:
staff_category:
  - preconstruction
things_you_should_know:
  - >-
    In his free-time, Erik coaches his son's hockey team and enjoys outdoor
    activities with his family including fishing and snowmobiling.
  - >-
    Best career advice Erik has received: "Don't be afraid to go outside of your
    comfort zone."
  - Ice cream cake is his guilty pleasure.
---

As a Senior Estimator with 16 years of experience, Erik's greatest strength is his ability to develop a deep and thorough understanding of a customer’s goals and a project’s true scope. With a driving need to build construction solutions that exceed expectations, Erik delivers both value and quality.
