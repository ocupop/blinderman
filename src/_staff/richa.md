---
layout: bio
name: Richard Wrigley
job_title: Senior Project Manager
professional_image: /uploads/wrigley-r-bw-small-1.jpg
personal_image: /uploads/wrigleyr-bio.jpg
professional_affiliations:
  -
education:
  - achievement: 'B.A., Architectural Studies'
    institution: 'Liverpool John Moores University (Liverpool, UK)'
  - achievement: OSHA 30-hour
    institution:
  - achievement: First Aid/CPR/AED
    institution:
staff_category:
  - operations
things_you_should_know:
  -
---

Richard manages the construction process for multiple projects from point of award through project close-out by creating true partnerships with his fellow team members and project stakeholders.&nbsp;
