---
layout: bio
name: Mark Williams
job_title: Internship Participant
professional_image: /uploads/mwilliams-bw-small.png
personal_image:
professional_affiliations:
  -
education:
  - achievement: B.S., Civil Engineering (expected 2023)
    institution: Marquette University
staff_category:
  - operations
things_you_should_know:
  - >-
    One of Mark's proudest accomplishments is his sports radio show with his
    college roommates.
  - Mark has two pet rabbits at home, Chuckles and Thumper.
  - When Mark was growing up, he wanted to be a professional baseball player.
---

As a Blinderman Intern, Mark's work spans a variety of unique assignments. He primarily works within our Federal Division, with a focus on our US Army Corps of Engineers and National Park Service projects. Outside of the Federal Division, Mark spends time rotating between other company divisions, allowing exposure to a variety of opportunities within the construction industry.
