---
layout: bio
name: Conor Malaney,  LEED AP BD+C
job_title: Senior Project Manager
professional_image: /uploads/malaney-c-bw.jpg
personal_image: /uploads/malaney-conor.jpg
professional_affiliations:
  -
education:
  - achievement: B.S., Civil Engineering, with emphasis on Construction Management
    institution: University of Wisconsin – Madison
  - achievement: >-
      U.S. Army Corps of Engineers Construction Quality Management for
      Contractors
    institution:
  - achievement: OSHA 30-hour
    institution:
  - achievement: AED/CPR/FirstAid
    institution:
staff_category:
things_you_should_know:
  - >-
    The best career advice Conor ever received: Always push yourself to continue
    learning. Never settle.
  - >-
    Not one to settle for what is routine or comfortable, Conor is always up for
    trying out a new restaurant, and always on the hunt for the next “it” band.
  - Conor can recite 87% of the lyrics to Shoop by Salt-N-Pepa.
---

Conor manages the construction process for multiple projects from point of award through project close-out by creating true partnerships with his fellow team members and project stakeholders. Conor believes active listening that includes not just hearing but understanding what is being communicated is key to the ultimate success of any construction project.
