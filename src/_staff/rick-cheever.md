---
layout: bio
name: Rick Cheever
job_title: Quality Control Manager
professional_image: /uploads/cheeverr-bw-small.jpg
personal_image:
professional_affiliations:
  -
education:
  - achievement: OSHA 30-hour
    institution:
  - achievement: AED/CPR/First Aid
    institution:
staff_category:
  -
things_you_should_know:
  -
---

&nbsp;

As Quality Control Manager, Rick is responsible for planning, directing, and implementing a quality control program in compliance with owner contract and company requirements.
