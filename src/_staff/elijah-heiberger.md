---
layout: bio
name: Elijah Heiberger
job_title: Project Engineer
professional_image: /heiberger-e-bw-small.jpg
personal_image: /uploads/heibergere-small.jpg
professional_affiliations:
  -
education:
  - achievement: B.S., Construction Management
    institution: South Dakota State University
  - achievement: OSHA 30-Hour
    institution:
  - achievement: AED/CPR/FirstAid
    institution:
staff_category:
  - operations
things_you_should_know:
  - Singapore is at the top of Elijah's travel list.
  - Elijah has never met an overpriced cup of coffee he doesn't love.
  - He has a natural ability to de-escalate situations.
---

Elijah Heiberger is a full-time Project Engineer responsible for providing administrative support, and on-site assistance for Project Managers.
