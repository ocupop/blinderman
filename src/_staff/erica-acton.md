---
layout: bio
name: Erica Acton
job_title: Assistant Project Manager
professional_image: /uploads/actone-bw-small.jpg
personal_image: /uploads/actone-bio.jpg
professional_affiliations:
  -
education:
  - achievement: 'M.E., Architectural Engineering '
    institution: Illinois Institute of Technology
  - achievement: 'B.S., Architectural Engineering '
    institution: Illinois Institute of Technology
  - achievement: OHSA 30-hour
    institution:
  - achievement: AED/CPR/First Aid
    institution:
staff_category:
  - operations
things_you_should_know:
  - >-
    Erica's favorite quote comes from the novel The Book Thief: "She took a step
    and didn't want to take any more, but she did."
  - >-
    Her nickname is Kramer and her family celebrates Christmas, known as
    "Festivus" in their home, every year
  - She collects keychains as a hobby
---

As Assistant Project Manager, Erica works directly with Project Managers and Executives, overseeing projects. She provides administrative support, facilitates owner and subcontractor communications, and assists on-site, as needed. Erica is also member of Blinderman's Diversity, Equity, and Inclusion (DEI) committee.
