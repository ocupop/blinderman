---
layout: bio
name: Damian Perez
job_title: Project Engineer
professional_image: /perez-d-bw-small.jpg
personal_image: /uploads/perezd-2021--smalljpg.jpg
professional_affiliations:
  -
education:
  - achievement: B.S., Applied Engineering and Technology
    institution: Eastern Illinois University
  - achievement: OSHA 30-Hour
    institution:
  - achievement: AED/CPR/FirstAid
    institution:
staff_category:
  - operations
things_you_should_know:
  - >-
    Best career advice Damian has ever received: ""Either run the day or the day
    runs you."
  - If he could travel anywhere, Damian would go to England.
  - When Damian was a kid he wanted to be a Chicago Cubs baseball player.
---

Damian Perez is a full-time Project Engineer responsible for providing administrative support, and on-site assistance for Project Managers.
