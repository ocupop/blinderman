---
layout: bio
name: Gerardo Mota
job_title: Assistant Superintendent
professional_image: /uploads/gmota-bw-small.png
personal_image: /uploads/gmota-color-small.jpg
professional_affiliations:
  - OSHA 30-hour
education:
  - achievement:
    institution:
staff_category:
  - operations
things_you_should_know:
  - >-
    Gerardo has four furry friends at home: dogs Jessy & Gizmo and cats Lilly &
    Pepper.
  - >-
    When he isn't at work, Gerardo can be found hanging out with his wife and
    kids.
  - Gerardo's favorite movie quote is, "Not all superheroes wear capes."
---

As Assistant Superintendent, Gerardo works closely with his team and looks ahead to anticipate obstacles, and solve problems. He is responsible for pushing a job forward, ensuring it is done right, on-time, and safely.
