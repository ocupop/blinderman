---
layout: bio
name: Brittany Long
job_title: Safety Director
professional_image: /uploads/long-b-bwjpg.jpg
personal_image: /uploads/long-brittany.jpg
professional_affiliations:
  -
education:
  - achievement: 'B.S., Marketing'
    institution: North Park University
  - achievement: Certified Journeywoman Carpenter
    institution:
  - achievement: Construction Safety Council 145-Hour Construction Safety Administrator Course
    institution:
  - achievement: UBC Supervisory Training
    institution:
  - achievement: OSHA 30-hour
    institution:
  - achievement: AED/CPR/First Aid
    institution:
  - achievement: US Army Corps of Engineers Construction Quality Management for Contractors
    institution:
staff_category:
  - operations
things_you_should_know:
  - 'When she’s not at work, Brittany’s cooking up sometimes-questionable experimental recipes. Experimentation pays off, though, as her culinary contributions to our Blinderman Potluck lunches are always most delicious.'
  - Brittany collects vintage housewares from the 1930s and 40s.
  - 'An ordained minister, Brittany officiates the weddings of friends and family – five so far.'
---


Through the development and implementation of our comprehensive corporate Safety & Health program, Brittany guides our staff – or anyone who may set foot on a Blinderman jobsite – in preventing and avoiding accidents, injuries and illness. Her goal is to help each person perform their job in such a way that both projects and the public are protected from harm, workers are physically able to continue working and retire in good health, and the fiscal well-being of Blinderman and its work partners is supported. Brittany’s focus on risk management and injury prevention is the engine that drives safety as a value throughout our company culture.