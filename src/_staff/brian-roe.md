---
layout: bio
name: Brian Roe
job_title: Project Manager
professional_image: /uploads/roeb-bw-small-1.jpg
personal_image: /uploads/roeb-bio.jpg
professional_affiliations:
  -
education:
  - achievement: 'B.S., Civil Engineering'
    institution: 'Purdue University – West Lafayette, Indiana'
  - achievement: OSHA 30-hour
    institution:
staff_category:
  - operations
things_you_should_know:
  - 'Most nights, Brian moonlights as a stand-up comedian after work.'
  - 'Best career advice Brian has ever received: "Never stop learning."'
  - Brian can recite most Dave Chappelle jokes.
---

As a Project Manager, Brian spends his time overseeing projects, hiring and approving work completed by subcontractors, negotiating contracts, and developing a budget and a timeline for project completion. He also works as a liaison between the construction team, architects, designers and the owners and stakeholders of the project to facilitate seamless communication, decision-making and pro-active problem-solving.