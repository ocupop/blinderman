---
layout: bio
name: Dave Guseman
job_title: Vice President, Federal Program
professional_image: /uploads/gusemand-bw-2019-small-1.jpg
personal_image: /uploads/guseman-biopic-1.jpg
professional_affiliations:
  -
education:
  - achievement: B.A., Psychology
    institution: University of Illinois at Chicago
  - achievement: Certified Journeyman Carpenter
    institution:
  - achievement: EPA Lead Renovator for Lead Safety in Remodeling, Repair and Painting
    institution:
  - achievement: OSHA 30-hour
    institution:
  - achievement: CPR/AED/First Aid
    institution:
staff_category:
  - leadership
things_you_should_know:
  - Yo ho, mateys! Dave wanted to grow up to be a Pirate. Aargh!
  - >-
    For the record, Dave can recite way too much of the dialog from Monty Python
    and the Holy Grail… and so it is no surprise that his favorite movie quote
    (which he is able to trot out quite often in his line of work) is: 
    “Perhaps... if we build a large wooden badger…” – Monty Python
  - >-
    Best career advice Dave ever received: Seize every moment of every day but
    when it’s time to go home... go home.
---

Dave leads our Federal Program at Blinderman Construction.&nbsp; His experience with the US Army and United Brotherhood of Carpenters, as a journeyman, foreman, and superintendent provide extensive knowledge of field operations.&nbsp; He successfully transitioned that knowledge to guide and lead our public sector work acquisition for several years until he was promoted to his current position. Dave’s positive energy, pragmatic counsel, and logical in-field viewpoint are valued contributions to Blinderman’s Executive & Strategic Planning teams.
