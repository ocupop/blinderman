---
layout: bio
name: Jack Hobbs
job_title: Senior Superintendent
professional_image: /uploads/hobbs-j-bw-web-4.jpg
personal_image: /uploads/hobbs-jack2.jpg
professional_affiliations:
  -
education:
  - achievement: Certified Journeyman Carpenter
    institution:
  - achievement: 'EPA Lead Renovator for Lead Safety in Remodeling, Repair and Painting'
    institution:
  - achievement: OSHA 30-hour
    institution:
  - achievement: CPR/AED/First Aid
    institution:
staff_category:
  - operations
things_you_should_know:
  - >-
    Jack collects historical memorabilia, the most outstanding piece in his
    collection being Abraham Lincoln’s signature.
  - 'A serious gamer, Jack is currently a big fan of attackofthecute.com'
  - >-
    The best career advice Jack ever received: Do what you believe is right… and
    do not waiver.
---

Jack has learned over the course of his 30+ years in the construction field that he thrives on managing challenging projects. With an entrepreneurial spirit that leads him to act with a tenacity and determination that gets the job done, Jack is at the top of his game when he needs to figure out the best solutions to complicated problems, then implement said solutions successfully with minimal impact to project cost and schedule objectives.