---
layout: bio
name: Bella Pulido
job_title: Assistant Project Manager
professional_image: /uploads/bpulido-bw-small.png
personal_image: /uploads/bpulido-color-small.png
professional_affiliations:
  -
education:
  - achievement: B..S., Systems Engineering and Design
    institution: University of Illinois at Urbana-Champaign
staff_category:
  - operations
things_you_should_know:
  - Growing up, Bella wanted to be a veterinarian when she got older.
  - >-
    One of Bella's hidden talents is her ability to recite all 50 US States in
    alphabetical order.
  - >-
    Bella has an adorable Chihuahua-Pomeranian-Pointed-Hair-Griffin pup named
    Poko.
---

As Assistant Project Manager, Bella serves as a liaison between the administrative team and the job site for the projects she works on. She builds and maintains relationships with stakeholders to aid in the timely and effective completion of construction projects.
