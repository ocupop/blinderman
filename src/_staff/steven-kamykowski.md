---
layout: bio
name: Steven Kamykowski
job_title: Project Manager
professional_image: /uploads/kamykowskis-2-bw-small-1.jpg
personal_image:
professional_affiliations:
  -
education:
  - achievement: General Education
    institution: University of Illinois Chicago
  - achievement: OSHA 30-hour
    institution:
  - achievement: AED/CPR/First Aid
    institution:
staff_category:
  -
things_you_should_know:
  -
---

As a Project Manager, Steven works as a liaison between the construction team, architects, designers and the owners and stakeholders of the project to facilitate seamless communication, decision-making and proactive problem-solving. He spends his time overseeing projects, hiring and approving work completed by subcontractors, negotiating contracts, and developing a budget and a timeline for project completion.
