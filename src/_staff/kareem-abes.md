---
layout: bio
name: Kareem Abes
job_title: Assistant Project Manager
professional_image:
personal_image:
professional_affiliations:
  - OSHA 30-hour
  - AED/CPR/First Aid
education:
  - achievement:
    institution:
staff_category:
  -
things_you_should_know:
  -
---
As Assistant Project Manager, Kareem works directly with Project Managers and Executives, overseeing projects. He provides administrative support, facilitates owner and subcontractor communications, and assists on-site, as needed.
