---
layout: bio
name: Natalie House
job_title: Marketing Coordinator
professional_image: /uploads/house-n-bw.jpg
personal_image: /uploads/nhouse-website-small.jpg
professional_affiliations:
  -
education:
  - achievement: B.S., Informatics – Social Computing
    institution: University of Michigan at Ann Arbor
  - achievement: AED/CPR/First Aid
    institution:
staff_category:
  - administration
things_you_should_know:
  - Natalie knows the Michigan fight song by heart.
  - >-
    Natalie was a competitive ballroom dancer for 10 years, although currently
    on a hiatus, she hopes to one day make her return to the dance floor.
  - >-
    Best career advice Natalie ever received: Go after a career that makes you
    want to get up in the morning.
---

As the Marketing Coordinator, Natalie’s day-to-day responsibilities are constantly shifting to meet business needs. She enjoys that her role is multi-faceted because it keeps things interesting. She is responsible for reviewing specifications and requirements for RFP and RFQ responses, assisting with creative and technical writing, preparing presentations, and assisting with employee engagement initiatives.
