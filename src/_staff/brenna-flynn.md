---
layout: bio
name: Brennan Flynn
job_title: Estimator
professional_image: /uploads/flynnb-bw-small-1.jpg
personal_image: /uploads/flynn-bio2.jpg
professional_affiliations:
  - OSHA 30-hour
  - AED/CPR/First Aid
education:
  - achievement:
    institution:
staff_category:
  - preconstruction
things_you_should_know:
  - >-
    When Brennan was a kid, we wanted to be an MLB player and an astronaut in
    the off-season.
  - He is most proud of renovating a 1950s house in his spare time.
  - >-
    Best career advice Brennan has received: "We learn wisdom from failure much
    more than from success."
---

With five years of experience, Brennan Flynn's preconstruction knowledge stems directly from general contracting, construction management, and self-perform carpentry estimating. As an Estimator, Brennan works alongside his team to put together scope packages and competitive pricing that encompasses the client's project expectations and budget.

Construction is a Flynn family trade, Brennan's father is a skilled craftsman that taught him everything he knows about woodworking and construction. The result? Brennan underscores the importance of maintaining strong working relationships with subcontractors in order to deliver the best possible price and services to our clients.&nbsp;