---
layout: bio
name: Kiara Ocasio
job_title: Assistant Project Manager
professional_image: /uploads/kocasio-bw-small-1.png
personal_image: /uploads/kocasio-small.png
professional_affiliations:
  -
education:
  - achievement: B.S., Psychology
    institution: United States Military Academy at West Point
  - achievement: M.S., Psychology
    institution: American Military University
staff_category:
  - operations
things_you_should_know:
  - >-
    If Kiara could travel anywhere in the world, she would go to Machu Picchu,
    Peru.
  - As a kid, Kiara wanted to be a Soldier when she grew up.
  - >-
    Joining the military was the single event in Kiara's life that created the
    biggest impact.
---

As Assistant Project Manager, Kiara serves as a liaison between the administrative team and the job site for the projects she works on. She builds and maintains relationships with stakeholders to aid in the timely and effective completion of construction projects in Fort Riley, Kansas.
