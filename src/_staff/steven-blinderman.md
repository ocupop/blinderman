---
layout: bio
name: 'Steven Blinderman, PE'
job_title: President / Chief Operations Officer
professional_image: /uploads/blinderman-s-formal-bw-test.jpg
personal_image: /uploads/blinderman-s-bio.jpg
professional_affiliations:
  - Hispanic American Construction Industry Association (HACIA)
  - 'Society of American Military Engineers (SAME), Fellow'
  - Association of Subcontractors and Affiliates (ASA Chicago)
education:
  - achievement: 'MBA, Finance'
    institution: 'University of Chicago, Booth School of Business'
  - achievement: >-
      M.S., Civil Engineering, (Geotechnical) & BS, Civil Engineering,
      (Structures)
    institution: University of Illinois at Urbana-Champaign
  - achievement: Professional Engineer
    institution: Illinois 062-044047
  - achievement: LEED AP
    institution:
  - achievement: OSHA 30-hour
    institution:
staff_category:
  - leadership
things_you_should_know:
  - >-
    The event that had the greatest impact on Steve’s life – the day Blinderman
    Construction was born and his dad commandeered the family ping pong table
    for use as a desk.
  - >-
    When he’s not at work, Steve commandeers the dining room table for use as a
    desk.
  - >-
    When Steve was a kid he dreamed of growing up to be a general contractor –
    which is such a pragmatic dream for Steve to have.
---

*“I believe our job is to transcend customer expectations – and we make a point of doing that, day in and day out.”*

As President/COO, Steve is directly responsible for project policy making, management overview, safety and most important – our clients’ absolute satisfaction with Blinderman’s staff, service, and performance. In addition to providing leadership, strategy and direction as President, Steve holds authority on all issues relating to management of our preconstruction consultation.

Growing up in a family owned business, Steve began his 30 plus-year career at Blinderman Construction working as a jobsite laborer. While continuing his undergraduate and graduate studies, he moved on to hold positions in estimating, project management and field supervision. His deep and nuanced understanding of the construction management world has proved to be invaluable in providing our clients with the singular care and smart solutions required for realizing certain success.

Over the last 15 years Steve has been an active member of the Lake Michigan Post of the Society of American Military Engineers (SAME) – a professional organization dedicated to fostering a public and private partnership in the interest of national security. He has served as a Board member, Treasurer and President of the Lake Michigan Post and has been instrumental in fundraising for scholarships and in organizing outreach events to educate and promote opportunities for small and disadvantaged businesses in the federal marketplace. In recognition of his dedicated and outstanding service to SAME and to military engineering, Steve has been bestowed with the designation of Fellow.

Steve has served the last ten years as a Board member and Board Chair of YouthBuild Lake County, a not-for-profit located in North Chicago which provides education and job training to at-risk youth.