---
layout: bio
name: Wyell Woods
job_title: Internship Participant
professional_image: /uploads/wwoods-bw-small-2.png
personal_image: /uploads/wwoods-color-small.png
professional_affiliations:
  - OSHA Certification
education:
  - achievement: B.S., Civil Engineering (expected 2023)
    institution: Purdue University
staff_category:
  - operations
things_you_should_know:
  - When Wyell was a kid, he wanted to be an architect when he grew up.
  - If he could build anything, Wyell would build a mansion out of hempcrete.
  - >-
    Wyell's favorite movie quote comes from The Lion King: "The past can
    hurt...But, you can either run from it, or learn from it."
---

As a Blinderman Intern, Wyell's work spans a variety of unique assignments. He primarily works within our Regional Division, with a focus on our Chicago Public School (CPS) projects. Outside of the Regional Division, Wyell spends time rotating between other company divisions, allowing exposure to a variety of opportunities within the commercial construction industry.
