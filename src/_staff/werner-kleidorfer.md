---
layout: bio
name: Werner Kleidorfer
job_title: Senior Superintendent
professional_image: /uploads/kleidorfer-w-small-1.jpg
personal_image:
professional_affiliations:
  - 'Build Safe Chicago, Cofounder'
education:
  - achievement: 'A.S., Construction Technology'
    institution: Triton College
  - achievement: OSHA 30-hour
    institution:
  - achievement: EM 385
    institution:
  - achievement: First Aid/CPR/AED
    institution:
staff_category:
  - operations
things_you_should_know:
  - >-
    Best career advice Werner ever received: "Stay in school and acquire a
    Construction Management degree."
  - 'If Werner isn''t at work, he''s probably playing hockey.'
  - Werner can easily binge eat pizza.
---


With 39 years of construction experience, Werner is highly skilled at providing overall leadership for on-site field administration, supervision and technical management for all construction operations. He has worked across the United States including projects in Illinois, Michigan and Colorado. As a Senior Superintendent, Werner’s considerable expertise is a true value-add, and he is highly skilled and adept at coordinating and executing of work on-time and within budget, maintaining a safe workplace, and enhancing client relations.