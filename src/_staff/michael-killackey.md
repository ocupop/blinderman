---
layout: bio
name: Michael Killackey
job_title: Superintendent
professional_image: /uploads/mkillackey-bw-small.png
personal_image:
professional_affiliations:
  -
education:
  - achievement:
    institution:
staff_category:
  -
things_you_should_know:
  -
---
As Superintendent, Michael works closely with his team and looks ahead to anticipate obstacles, and solve problems. He is responsible for pushing a job forward, ensuring it is done right, on-time, and safely.
