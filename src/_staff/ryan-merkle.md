---
layout: bio
name: Ryan Merkle
job_title: Superintendent
professional_image: /uploads/merkle-r-bw.jpg
personal_image: /uploads/merkle-r-bio2.jpg
professional_affiliations:
  -
education:
  - achievement: 'B.S., Civil Engineering'
    institution: University of Illinois at Urbana-Champaign
  - achievement: OSHA 30-hour
    institution:
  - achievement: CPR/AED/First Aid
    institution:
staff_category:
  - operations
things_you_should_know:
  - 'Best career advice Ryan received: Make time for your family.'
  - Ryan can easily binge eat peanut butter – by the spoonful.
---

With 5 years of experience, as superintendent Ryan works closely with his team and looks ahead to anticipate obstacles, and solve problems. He is responsible for pushing a job forward, ensuring it is done right, on-time, and safely. Ryan's past project experience includes complex renovations and ground-up construction in the hospitality, aviation, and federal markets.
