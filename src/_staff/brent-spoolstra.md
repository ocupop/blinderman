---
layout: bio
name: Brent Spoolstra
job_title: Preconstruction Director
professional_image: /uploads/spoolstra-b-bw.jpg
personal_image: /spoolstra-2021.jpg
professional_affiliations:
  -
education:
  - achievement: B.S., Construction Management, Engineering Technology
    institution: Purdue University Calumet
  - achievement: A.S., Architectural Engineering Technology
    institution: Purdue University Calumet
  - achievement: OSHA 30-hour
    institution:
staff_category:
  - preconstruction
things_you_should_know:
  - >-
    If Brent could build anything, he would build a mountain, castle, and a
    treehouse out of a molehill, sand, and a tree, respectively.
  - >-
    Brent’s favorite movie lines: “Surely, you can't be serious.” “I am serious…
    and don’t call me Shirley.” – Airplane!
  - >-
    Best career advice Brent ever received: If you don't love what you do for a
    living, it’s not worth doing.
---

As Preconstruction Director, Brent manages the production of estimates for all of Blinderman’s pursuits and the entire preconstruction team. His greatest strength is his ability to develop a deep and thorough understanding of a customer’s goals and a project’s true scope.
