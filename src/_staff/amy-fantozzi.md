---
layout: bio
name: Amy Fantozzi
job_title: Project Coordinator
professional_image: /uploads/fantozzia-bw-small-1.jpg
personal_image: /uploads/fantozzia-2-edit-small.jpg
professional_affiliations:
  - AED/CPR/First Aid
education:
  - achievement: M.A., Criminal Justice & Criminology
    institution: Loyola University Chicago
  - achievement: 'B.S., Criminal Justice & Criminology '
    institution: Loyola University Chicago
staff_category:
  - operations
things_you_should_know:
  - >-
    Amy rescued her sweet brindle puppy Addison from PAWS Chicago and also
    fosters dogs in her free time.
  - >-
    Receiving her Crisis Intervention Counselor certification during COVID and
    volunteering as a medical advocate are among the achievements of which she
    is most proud.
  - >-
    The best career advice Amy ever received is, "Never be too proud to ask
    questions."
---

Amy provides administrative support to Blinderman’s Operations staff on ongoing projects. As Project Coordinator, she is responsible for assisting with billing, research, and other administrative duties.
