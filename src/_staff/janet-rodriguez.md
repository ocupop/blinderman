---
layout: bio
name: Janet Magaña
job_title: Senior Project Accountant
professional_image: /uploads/rodriguez-j.jpg
personal_image: /uploads/rodriguez-janet-copy.png
professional_affiliations:
  -
education:
  - achievement: 'B.A., Corporate Communications'
    institution: Northern Illinois University
staff_category:
  - administration
things_you_should_know:
  - >-
    Though Janet collects Christmas ornaments, she accepts additions to her
    collection anytime of the year.
  - >-
    Having recently purchased her first house, Janet is addicted to home
    improvement programs.
  - >-
    Pizza is Janet’s all-time very favorite food so it comes in handy that her
    family owns a pizza parlor.
---

Janet collaborates with the CFO and Project Managers in her role as Senior Project Accountant. She organizes and maintains the details of each our project accounts including estimates, subcontracts, purchase orders, trade contract files, certified payrolls, lien waivers and EEO/M/W/DBE reports.
