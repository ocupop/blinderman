---
layout: bio
name: Tony Stern
job_title: Assistant Project Manager
professional_image: /stern-t-bw-small.jpg
personal_image: /uploads/sternt.jpg
professional_affiliations:
  -
education:
  - achievement: Production Management Certificate
    institution: Moraine Valley Community College
  - achievement: OSHA 30-Hour
    institution:
  - achievement: AED/CPR/First Aid
    institution:
staff_category:
  - operations
things_you_should_know:
  - >-
    Tony is most proud of getting his daughter involved in woodworking, they
    make furniture together for their home.
  - Visiting Machu Picchu in Peru is at the top of Tony's travel list.
  - His guilty pleasure is binge watching episodes of The Office.
---

As Assistant Project Manager, Tony works directly with Project Managers and Executives, overseeing projects. He provides administrative support, facilitates owner and subcontractor communications, and assists on-site, as needed.&nbsp;
