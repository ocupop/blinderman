---
layout: bio
name: David Culcasi
job_title: Project Executive
professional_image: /uploads/culcasi-d-bw.jpg
personal_image: /uploads/culcasi-dave.jpg
professional_affiliations:
  -
education:
  - achievement: 'B.S., Civil Engineering, Graduated Cum Laude'
    institution: University of Notre Dame
  - achievement: LEED AP
    institution:
  - achievement: OSHA 30-hour
    institution:
  - achievement: AED/CPR/FirstAid
    institution:
staff_category:
  - operations
things_you_should_know:
  - Dave served seven years in the United States Navy (Civil Engineer Corps).
---

Over the course of his 15+ year career, Dave has managed all aspects of active construction projects from conception through closeout. His project experience ranges from small to large of both new construction and renovation projects. Dave is also serves as Blinderman’s Viewpoint Guru, always willing to share his vast mastery of Blinderman’s project management software with his fellows.