---
layout: bio
name: Chris Hill
job_title: Quality Control Manager
professional_image: /uploads/chill-bw-small.png
personal_image: /uploads/20180603-175100.jpg
professional_affiliations:
  - OSHA 30-hour
  - AED/CPR/First Aid
education:
  - achievement: B.A., History
    institution: University of North Carolina at Charlotte
  - achievement: Civil Engineer Certificate
    institution: Central Piedmont Community College
staff_category:
  -
things_you_should_know:
  - Chris can recite his High School Alma Mater by heart.
  - When he was a child, Chris wanted to be a soldier when he grew up.
  - Chris is most proud of his sons, Jonathan and Joshua.
---

