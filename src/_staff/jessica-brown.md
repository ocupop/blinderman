---
layout: bio
name: Jessica Brown
job_title: Project Coordinator
professional_image: /uploads/jbrown-bw-small.png
personal_image: /uploads/pic-3.jpg
professional_affiliations:
  -
education:
  - achievement:
    institution:
staff_category:
  - operations
things_you_should_know:
  - Jessica has a dog named Mia and a cat named Simba at home.
  - When she was a kid, Jessica wanted to be a nurse when she grew up.
  - >-
    When Jessica isn't at work, you can find her running the local Flag Football
    and Cheer League—or at one of her kids' sporting events.
---

Jessica provides administrative support to Blinderman’s Operations staff for ongoing projects. As Project Coordinator, she is responsible for assisting with billing, research, and other administrative duties.
