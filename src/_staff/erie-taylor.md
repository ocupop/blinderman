---
layout: bio
name: Erie Taylor
job_title: Payroll Specialist
professional_image: /uploads/taylor-e.jpg
personal_image: /uploads/taylor-erie.jpg
professional_affiliations:
  -
education:
  - achievement: 'B.S., Business'
    institution: Illinois State University
staff_category:
things_you_should_know:
  - >-
    If he could build anything, Erie would build a house using sandbag
    technology. Google it.
  - >-
    Erie’s self-admitted guilty pleasure: Chicago Bulls lower level seating
    tickets.
  - >-
    The movie quote that resonates for Erie is a classic: “May the force be with
    you.” – Star Wars
---

Erie is the man who makes sure we all get paid. He interacts with all cost accounting applications, including job cost, general ledger, and payroll. He also assists with database administration duties for our Viewpoint project management and accounting software.
