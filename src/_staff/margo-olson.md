---
layout: bio
name: Margo Olson, AIA, LEED AP BD+C
job_title: Project Executive
professional_image: /uploads/olson-m-bw-cropped.jpg
personal_image: /uploads/olson-margo2.jpg
professional_affiliations:
  - AIA Certified
education:
  - achievement: M.S., Architecture
    institution: University of Illinois at Chicago
  - achievement: B.S., Architecture
    institution: University of Minnesota
  - achievement: OSHA 30-hour
    institution:
staff_category:
  - operations
things_you_should_know:
  - >-
    At age 20, Margo attended a presentation by Chicago architect Doug Garofalo
    on how construction influenced architecture – she was thereafter, hooked on
    construction.
  - >-
    One day Margo would like to design and build a fantastic futuristic house
    using cradle-to-cradle materials.
  - >-
    Best career advice Margo ever received: “You have to ask the right question
    to get the right answer.” – Margo’s Dad
---

Margo works each day to further relationships with our clients and execute projects that we can all be proud of. As an architect, she understands the design and construction processes, and her management style involves being a sensitive and effective bridge between the two. Margo adds her unique voice to Blinderman’s Strategic Planning team. She is also member of Blinderman's Diversity, Equity, and Inclusion (DEI) committee.
