---
layout: bio
name: Jenna Courchane
job_title: Estimator
professional_image: /uploads/courchane-j-bw-small.jpg
personal_image: /uploads/courchanej-bio.jpg
professional_affiliations:
  -
education:
  - achievement:
    institution:
  - achievement: 'B.S., Civil and Environmental Engineering'
    institution: Northwestern University
staff_category:
  - preconstruction
things_you_should_know:
  - >-
    Best career advice Jenna has received: Do it before you feel ready to. If
    you wait until you're ready, you'll never try anything new.
  - >-
    Jenna is an avid traveler and picks up a deck of cards from every city
    before departing.
  - >-
    If you're looking for her outside of the office, she's probably in Wisconsin
    or at a baseball game - sometimes both!
---

With seven years of experience, Jenna has fine-tuned her ability to develop a deep and thorough understanding of a customer’s goals and a project’s true scope. She works closely with Preconstruction team members to develop value-engineered estimates while maintaining relationships with subcontractors to secure best pricing and services.