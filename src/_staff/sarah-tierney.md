---
layout: bio
name: Sarah Tierney
job_title: Marketing Administrative Assistant
professional_image: /uploads/stierney-bw-small-1.png
personal_image: /uploads/tierneys-website-small.jpg
professional_affiliations:
  -
education:
  - achievement: M.F.A., Writing (Fiction)
    institution: Columbia University in the City of New York
  - achievement: B.A., English (Creative Writing)
    institution: DePaul University
  - achievement: AED/CPR/First Aid
    institution:
staff_category:
  -
things_you_should_know:
  - >-
    Sarah's proudest accomplishment was writing her first short story
    collection.
  - >-
    When she was a child, it was her dream to become a Broadway Star and New
    York Times Best-selling Author
  - Sarah's seven-year-old cat Olivia is the light of her life.
---

Sarah provides administrative services in support of the Marketing team by securing bid documents, assembling bid forms, submitting bids, compiling and analyzing data, and maintaining bid documents. She assists with creative and technical writing in her support of acquisition and marketing efforts. Sarah’s central role is critical to ensuring the accuracy and timeliness of our team’s work acquisition efforts.
