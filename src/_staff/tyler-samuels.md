---
layout: bio
name: Tyler Samuels
job_title: Project Administrator
professional_image: /uploads/samuelst-bw-small-1.png
personal_image: /uploads/samuelst-small.png
professional_affiliations:
  - AED/CPR/First Aid
  - OSHA 30-hour
education:
  - achievement: A.S. in Architectural Studies
    institution: Southern Illinois University
  - achievement: 3G Welding Qualification
    institution: Joliet Junior College
staff_category:
  - operations
things_you_should_know:
  - >-
    When he's not at work, Tyler can be found mixing house tracks on his CDJ
    850s.
  - >-
    One of Tyler's proudest accomplishments is writing his first business plan
    during the pandemic.
  - Tyler uses an Excel spreadsheet to keep track of his tool collection.
---

As a Project Administrator, Tyler is responsible for various administrative duties surrounding a project. These duties include documentation, meeting management, handling project budgets, and using time management skills to help the team stay on track. Tyler helps Blinderman achieve the ultimate goal of ensuring projects are completed within the restraints of time, quality, and budget.
