---
layout: bio
name: David Blinderman
job_title: CHIEF EXECUTIVE OFFICER
professional_image: /uploads/blindermand-bw.jpg
personal_image: /uploads/blindermand-bio.jpg
professional_affiliations:
  - The Economic Club of Chicago
  - 'Committee Member, Society of Contemporary Art Member and Gala Host'
  - 'Board Member, Chicago International Charter Schools'
education:
  - achievement: 'MBA, Finance, Real Estate & Marketing'
    institution: Northwestern University - Kellogg School of Management
  - achievement: 'B.S., Science, Civil Engineering (Structural & Construction Management)'
    institution: University of Illinois Urbana-Champaign
staff_category:
  - leadership
things_you_should_know:
  - David bakes insanely delicious German Chocolate Cake from scratch.
  - >-
    David's favorite movie quote: 'I guess it comes down to a simple choice,
    really. Get busy living or get busy dying.' - Andy Dufresne, Shawshank
    Redemption
  - >-
    Though he always tries to, David never quite manages to reach his goal of 20
    skiing days for the year. Hard to accomplish when you're running a growing
    business and also happen to live in the second flattest state in the
    country.
---

*“When I'm doing my job well, I'm empowering the incredible team we've assembled, and then I get out of their way.”*

David delivers singular care, smart solutions and certain success to private and public sector clients throughout the United States. As Chief Executive Officer, he establishes corporate strategy while providing leadership and direction to Blinderman Construction.

David began his career as a design engineer for one of Chicago’s largest structural engineering firms, assisting in the design of several prominent high-rise buildings and highway projects. Prior to Blinderman Construction, David worked as a portfolio manager for Aetna Real Estate Investors, a senior consultant for Ernst & Young specializing in real estate securitization and valuation and as the assistant director of development for a national real estate development firm based in Chicago.

David is defined by his enduring desire to make a positive impact in business and in the community. As a firm believer in access to quality public education, David is a strong promoter and supporter of STEM-centric career development and mentoring for underserved student populations. He is currently a member of the Board of Directors for Chicago International Charter School, a vibrant network of unique Chicago charter schools that prepares students to thrive in college and life, and empowers educators to innovate and lead.