---
layout: bio
name: Kasia Wodzisz
job_title: Internship Participant
professional_image: /uploads/kwodzisz-bw-small-1.png
personal_image: /uploads/kwodzisz-color-small.png
professional_affiliations:
  -
education:
  - achievement: B.Arch., Architecture (expected 2024)
    institution: Illinois Institute of Technology
staff_category:
  -
things_you_should_know:
  - >-
    When Kasia isn't at work or school, she can be found pursuing new
    collections and hobbies on the regular.
  - >-
    One of Kasia's proudest moments was submitting a comic for her final studio
    presentation.
  - When she was growing up, Kasia wanted to be a fashion designer.
---

Kasia’s internship spans a variety of engaging and unique assignments. She primarily works within the Preconstruction department, learning the ins and outs of our estimating process. Outside of Preconstruction, Kasia spends time rotating between company divisions, allowing exposure to a variety of opportunities within the construction industry.
