---
layout: bio
name: Thomas Lathrop
job_title: Concrete Division Leader
professional_image: /uploads/lathrop-t-bw-small.jpg
personal_image: /uploads/lathrop-tom.jpg
professional_affiliations:
  -
education:
  - achievement: OSHA 30-hour
  - achievement: AED/CPR/First Aid
  - achievement: ECATTS Environmental Compliance Assessment Training & Tracking System
  - achievement: Gypsum Installers License, Hacker Industries
  - achievement: Butterfield Stamped Concrete Training
  - achievement: Concrete Training OPCMIA
  - achievement: System One Training, Ardex
staff_category:
  - operations
things_you_should_know:
  - >-
    Tom would design and build an entire house of concrete, if he wasn’t busy
    running our concrete division.
  - >-
    The best career advice Tom ever received is: Never second guess your
    decisions.
---

As the Division Leader for Blinderman Concrete – our self-performed concrete division – Tom leverages his 30 years of construction industry experience to develop and grow a self-perform concrete group that is preeminent in the market. This includes marketing to and meeting with perspective clients, estimating work, keeping up with technological advances, and overseeing all aspects of structural concrete and repair.
