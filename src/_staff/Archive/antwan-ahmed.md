---
layout: bio
name: Antwan Ahmed
job_title: Project Engineer
professional_image: /uploads/1ahmeda-bw-small.jpg
personal_image: /uploads/ahmeda-biopic.jpg
professional_affiliations:
  -
education:
  - achievement: 'B.S., Mechanical Engineering'
    institution: Illinois Institute of Technology
  - achievement: OSHA 30-Hour
    institution:
  - achievement: QA/QC Army Corps Training
    institution:
  - achievement: First Aid/CPR/AED
    institution:
  - achievement: Project Engineer Procore Certified
    institution:
staff_category:
  - operations
things_you_should_know:
  - >-
    The best career advice Antwan has ever received: Keep in mind that how you
    do anything is how you do everything.
  - Antwan has two pet cats named Mitsy and Gatsby.
  - 'When he was a kid, he wanted to be a doctor.'
---

Antwan began his career at Blinderman as part of our intern program. Now, as Project Engineer, he works directly with Project Managers providing administrative support and on-site assistance when needed. Antwan is also member of Blinderman's Diversity, Equity, and Inclusion (DEI) committee.
