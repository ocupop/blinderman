---
layout: bio
name: Daniel Caron
job_title: Project Manager
professional_image: /caron-d-bw-small.jpg
personal_image: /uploads/carond-pic.jpg
professional_affiliations:
  -
education:
  - achievement: B.S., Applied Engineering and Technology
    institution: Eastern Illinois University
  - achievement: OSHA 30-Hour
    institution:
staff_category:
  - preconstruction
things_you_should_know:
  - When Dan isn't at work, he's either golfing or fishing.
  - He has a dog named Gus.
  - When he was a kid, he wanted to be an NFL player.
---

As a Project Manager with five years of experience, Daniel is responsible for overseeing projects, hiring and approving work completed by subcontractors, negotiating contracts, and developing a budget and a timeline for project completion. He also works as a liaison between the construction team, architects, designers and the owners and stakeholders of the project to facilitate seamless communication, decision-making and pro-active problem-solving.
