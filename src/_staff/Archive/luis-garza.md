---
layout: bio
name: Luis Garza
job_title: Project Manager
professional_image: /uploads/garza-l-bw-small.jpg
personal_image: /uploads/lgarza-website-small.png
professional_affiliations:
  -
education:
  - achievement: B.S., Construction Management
    institution: Illinois State University
  - achievement: OSHA 30-hour
    institution:
  - achievement: AED/CPR/First Aid
    institution:
staff_category:
  -
things_you_should_know:
  - When Luis is not at work, he can be found coaching or playing volleyball.
  - >-
    Luis has two massive dogs—a Redbone Coonhound, Red, and a Great Dane,
    Darcie.
  - >-
    As a big Cubs fan, one of the greatest events Luis was able to witness was
    their 2016 World Series win.
---

As a Project Manager, Luis oversees projects, hires and approves work completed by subcontractors, negotiates contracts, and develops a budget and a timeline for project completion. He also works as a liaison between the construction team, architects, designers as well as the owners and stakeholders of the project to facilitate seamless communication, decision-making and proactive problem-solving.
