---
layout: bio
name: Christine Blevins
job_title: 'VP – Marketing Acquisitions '
professional_image: /uploads/blevins-c.jpg
personal_image: /uploads/blevins.jpg
professional_affiliations:
  -
education:
  - achievement: Harrington Institute of Interior Design
    institution:
staff_category:
  - leadership
things_you_should_know:
  - >-
    As a kid, Chris wished she could be a Time Traveler. As an adult, she wrote
    and published 3 historical fiction novels set in the 18th century.
  - >-
    Getting accidentally stabbed in the arm with an Exacto knife by a coworker
    was a life changing event for Chris.  She ended up marrying the stabber, and
    40 years post-stabbing, she’s still married to him.
  - >-
    Best career advice ever received: "Do or do not. There is no try." (OK, it's
    Yoda – but it's still good advice.)
---

Chris directs and manages how Blinderman Construction communicates who we are, what we do, and why we do it. As a longtime member of both Blinderman's Leadership and Strategic Planning teams, Chris adds an untraditional voice and viewpoint to the continued development and deployment of company strategy, culture, and policy.
