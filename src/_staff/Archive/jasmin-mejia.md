---
layout: bio
name: Jasmin Mejia
job_title: Marketing Manager
professional_image: /uploads/mejia-j-bw.jpg
personal_image: /uploads/mejia-bio.jpg
professional_affiliations:
  -
education:
  - achievement: M.S., Communication
    institution: Illinois State University
  - achievement: B.S., Public Relations
    institution: Illinois State University
staff_category:
  - administration
things_you_should_know:
  - >-
    Jasmin’s pet buddy is a miniature schnauzer named Dizzy, with a serious
    beard.
  - >-
    Jasmin is the chief organizer of Blinderman events including the annual
    fundraiser, Blindy Awards Dinner, and quarterly staff events.
  - 'Best career advice ever received: “You should always be learning.”'
---

Jasmin manages Blinderman's internal and external communication - her messaging shares who we are, what we do, and why we do it. She collaborates daily with c-level executives, including our leading Vice Presidents for the Federal and Regional programs, on various initiatives and process improvement. As a champion of our culture, Jasmin has a hand in onboarding, recruiting, employee engagement, and social responsibility. Jasmin has led Blinderman's Diversity, Equity, and Inclusion (DEI) committee since 2020.
