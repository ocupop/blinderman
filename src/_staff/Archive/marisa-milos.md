---
layout: bio
name: Marisa Milos
job_title: Project Coordinator
professional_image: /uploads/milos-m-bw-small.jpg
personal_image: /uploads/milosm-bio.jpg
professional_affiliations:
  -
education:
  - achievement: 'B.S., International Business'
    institution: Loyola University of Chicago
  - achievement: OHSA 30-hr
    institution:
staff_category:
  - operations
things_you_should_know:
  - >-
    Marisa collects handmade jewelry from places she has traveled to, and she
    also collects pins.
  - 'Best career advice received: "Work to live, don''t live to work."'
  - >-
    Marisa's guilty pleasure: Rewatching episodes of shows like Friends and The
    Office, even if she's seen them more than 100 times.
---

As Project Coordinator, Marisa provides administrative support to Blinderman’s Operations staff on ongoing projects. She is also responsible for assisting with billing, research, and other administrative duties.