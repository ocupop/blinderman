---
layout: bio
name: Steven Colberg
job_title: Project Executive
professional_image: /uploads/colberg-s-bw.jpg
personal_image: /uploads/colberg-steve.jpg
professional_affiliations:
  -
education:
  - achievement: B.S., Architecture
    institution: California Polytechnic State University, San Luis Obispo
  - achievement: LEED AP
    institution:
  - achievement: Certified Construction Manager
    institution:
  - achievement: OSHA 30-hour
    institution:
  - achievement: AED/CPR/First Aid
    institution:
staff_category:
  - operations
things_you_should_know:
  - >-
    One of those kids who loved to play with Lego, Steve caught the design-build
    bug early on (and he still likes to play with Lego).
  - >-
    When Steve is not at work, he’s in his wood shop making handcrafted
    furnishings of his own design.
  - Steve commutes to work on his 2000 Honda VFR 800 – weather permitting.
---

With close to 10 years in the construction industry, Steve’s range of experience allows him to criss-cross the boundaries between Operations and Preconstruction. Steve’s comprehensive expertise is put to best use as a contributor on design-build efforts, where his background as an architect and his training as an estimator meet to provide best value for our customer. Steve is also member of Blinderman's Diversity, Equity, and Inclusion (DEI) committee.
