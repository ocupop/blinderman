---
layout: bio
name: Marissa Kozen
job_title: Human Resources Manager
professional_image: /uploads/mkozen-bw-edit-small.jpg
personal_image: /uploads/mkozen-small-1.jpg
professional_affiliations:
  -
education:
  - achievement: B.A., Communication & Rhetoric
    institution: University of Pittsburgh
staff_category:
  -
things_you_should_know:
  - >-
    One of Marissa's proudest accomplishments is being the first member of her
    family to attend a 4-year college.
  - >-
    Marissa's favorite television quote comes from Park & Recreation's Leslie
    Knope: "We need to remember what's important in life: friends, waffles, and
    work."
  - >-
    When she isn't at work, Marissa can be found  with her pups Daisy and Pippa
    or exploring Chicago with her husband.
---

As Blinderman's Human Resources Manager, Marissa handles anything and everything people-related. She works alongside the Leadership team to provide support to our staff, lead recruitment efforts, and spearhead internal development, performance management, and Diversity, Equity, and Inclusion (DEI) initiatives.
