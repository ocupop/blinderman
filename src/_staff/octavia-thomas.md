---
layout: bio
name: Octavia Thomas
job_title: Accounts Receivable & Payable Specialist
professional_image: /thomas-o-bw2-small.jpg
personal_image: /uploads/octavia-website-small.jpg
professional_affiliations:
  - AED/CPR/First Aid
education:
  - achievement: A.S., Early Childhood Development
    institution: Kaplan University
staff_category:
  - administration
things_you_should_know:
  - >-
    Becoming a mom is the the single event that has had the most impact on
    Octavia's life.
  - Octavia can recite the entire Lion King movie by heart.
  - >-
    Being the first college graduate on her dad's side of the family is one of
    her proudest achievements.
---

At Blinderman, Octavia manages all incoming invoices and works in support of the CFO and project accountant. Octavia also works closely with our project managers to issue subcontractors payments. Aside from that, she is most likely to be the person to greet you at the front desk and manages the daily flow of the office.
