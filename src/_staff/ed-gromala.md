---
layout: bio
name: Edward Gromala
job_title: Project Executive
professional_image: /uploads/gromala-e-bw.jpg
personal_image: /uploads/gromala-ed.jpg
professional_affiliations:
  -
education:
  - achievement: 'B.S., Civil Engineering'
    institution: University of Illinois at Urbana-Champaign
  - achievement: OSHA 30 hour
    institution:
  - achievement: AED/CPR/First Aid
    institution:
staff_category:
  - operations
things_you_should_know:
  - 'An avid sportsman, Ed loves to run, coach kid soccer, play softball, and golf.'
  - 'With a nickname like Mr. 3 Par, and favorite colors of blue and orange (GO Illini!), it’s safe to say Ed enjoys sports.'
  - 'Ed’s favorite maxim: Knowledge is power.'
---


Besides being responsible for overall performance and completion of his projects, Ed ensures that his project team members have the resources and ability to achieve project goals and requirements. Ed best leverages his close to 40 years of experience working at Blinderman when he is teaching and mentoring his team members to be the best managers of our customers’ interests. Ed’s approach as a leader reflects his belief that lasting and productive relationships are built on a foundation of trust and respect.