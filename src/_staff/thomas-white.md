---
layout: bio
name: Thomas White
job_title: Chief Financial Officer
professional_image: /uploads/white-t-bw.jpg
personal_image: /uploads/white-tom.jpg
professional_affiliations:
  - Construction Financial Management Association (CFMA)
education:
  - achievement: 'BBA, Accounting'
    institution: Loyola University of Chicago
  - achievement: 'MBA, Finance'
    institution: DePaul University of Chicago Kellstadt School of Business
staff_category:
  - leadership
things_you_should_know:
  - 'When Tom was a kid, he wanted to be Chicago Cubs baseball player.'
  - 'Tom collects vintage baseball cards. He once had a Steve Garvey/Padres card, but he ripped it to shreds on October 7, 1984.'
  - "Tom’s favorite movie quote is very relevant: \"You're Mr. White, you have a cool sounding name.\" – Reservoir Dogs"
---


As CFO, Tom is responsible for the finance, accounting and treasury functions of the company.  He is a member of both the Leadership Team and Strategic Planning Team, providing critical input for managing the development and implementation of the company’s cultural and strategic goals.