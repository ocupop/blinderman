---
layout: bio
name: James Pokracki
job_title: Project Executive
professional_image: /uploads/pokracki-j-bw.jpg
personal_image: /uploads/pokracki-james.jpg
professional_affiliations:
  -
education:
  - achievement: Architecture Program
    institution: Olive Harvey College
  - achievement: Certified Journeyman Carpenter
    institution:
  - achievement: >-
      U.S. Army Corps of Engineers Construction Quality Management for
      Contractors
    institution:
  - achievement: EPA Lead Renovator for Lead Safety in Remodeling, Repair and Painting
    institution:
  - achievement: >-
      EPA Lead Renovator for Lead Safety in Remodeling, Repair and Painting ACOE
      EM 385-1-1 Safety & Health Requirements Training
    institution:
  - achievement: OSHA 30-hour
    institution:
  - achievement: AED/CPR/First Aid
    institution:
staff_category:
  - operations
things_you_should_know:
  - When Jim was a kid he wanted to be an architect – go figure.
  - >-
    Jim would love to design and build a Frank Lloyd Wright inspired home of
    sustainable materials.
  - >-
    Jim is most proud of his latest invention, the Lasergizmo® (patent pending)
    the details of which will be divulged once the patent is no longer pending.
---

Jim has over 35 years of experience orchestrating complex construction projects in the field. A master of the details, a consummate craftsman, and a natural leader, Jim’s greatest skill lies in his ability to truly partner with his customer, his project team, and subcontractors to deliver success on the most challenging projects. A real innovator, Jim is always seeking out new solutions and developing technologies in the construction industry to improve safety and productivity. These attributes and more make Jim a vital member of Blinderman’s Strategic Planning Team.
