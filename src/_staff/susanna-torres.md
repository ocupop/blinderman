---
layout: bio
name: Susanna Torres
job_title: Assistant Superintendent
professional_image: /uploads/torres-s-small.jpg
personal_image: /uploads/torres-s-bio.jpg
professional_affiliations:
  -
education:
  - achievement: Certified Journeywoman Carpenter
    institution:
  - achievement: >-
      Certified Competent Person: Fall Protection, Operator Training, Boom &
      Scissor Lifts
    institution:
  - achievement: 'EPA Lead Renovator for Lead Safety in Remodeling, Repair and Painting'
    institution:
  - achievement: OSHA 30-hour
    institution:
  - achievement: AED/CPR/First Aid
    institution:
staff_category:
  - operations
things_you_should_know:
  -
---


Sue Torres has developed a strong reputation as a proactive manager of site safety on complex and phased renovation projects in operational facilities. In addition to her role as Safety Manager, she is skilled at overseeing and coordinating subcontractors to meet project goals. Sue values collaboration as the means to ensure each project is completed on schedule and within budget.