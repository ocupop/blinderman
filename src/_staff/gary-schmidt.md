---
layout: bio
name: Gary Schmidt
job_title: Senior Superintendent
professional_image: /uploads/schmidtg-bw-web.jpg
personal_image: /uploads/schmidt.jpg
professional_affiliations:
  -
education:
  - achievement: 'A.S., Applied Science'
    institution: College of DuPage
  - achievement: Certified Journeyman Carpenter
    institution:
  - achievement: OSHA 30-hour
    institution:
  - achievement: CPR/AED/First Aid
    institution:
staff_category:
  - operations
things_you_should_know:
  - 'Gary loves to snowmobile, because cold and snowy is his thing.'
  - >-
    The acorn does not fall far from the oak. Gary’s dad was the first employee
    at the founding of Blinderman Construction back in 1972, hired on as a
    Superintendent.
  - Food that makes Gary happy? Portillo’s cheeseburger.
---


With Blinderman for over 27 years, Gary’s honed his craft on projects ranging from complex deadline-driven renovations to multi-million dollar LEED certified ground-up construction. A true collaborator and accomplished problem-solver, Gary has the razor-sharp skillset necessary to keep the complicated parts and pieces of a project moving in the right direction at the proper speed. Gary is routinely assigned to Blinderman’s most challenging projects, where he always works to exceed customer’s goals and expectations.