---
layout: event
title: Food Truck Fest
headline:
event_date: 2016-10-13 00:00:00
event_date_final: true
start_time: '4:00 pm'
end_time: '7:00 pm'
sponsorship_start_date: 2016-06-01 00:00:00
sponsorship_deadline_date: 2016-09-16 00:00:00
purchase_tickets_start_date: 2016-08-01 00:00:00
location_name:
address:
poster_image: /uploads/ftf-stamp.jpg
ticket_link:
show_sponsors: false
video_thumbnail:
video_url:
beneficiary:
beneficiary_description_html:
beneficiary_images:
  -
tier_sponsors:
  - tier_name:
    tier_description:
    tier_order:
    sponsors:
      - sponsor_name:
        sponsor_logo_image:
        sponsor_url:
carousel_images:
  - image:
---

