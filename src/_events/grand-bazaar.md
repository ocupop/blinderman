---
layout: event
title: Grand Bazaar
headline: 'Blinderman''s 4th Annual inSTEM Fundraiser'
event_date: 2019-10-24 00:00:00
event_date_final: true
start_time: '5:00 pm'
end_time: '8:00 pm'
sponsorship_start_date: 2019-06-01 00:00:00
sponsorship_deadline_date: 2019-09-13 00:00:00
purchase_tickets_start_date: 2019-09-18 00:00:00
location_name: The Dalcy
address: '302 N. Green Street, 3rd Floor, Chicago, IL'
poster_image: /uploads/bazaar-stamp.png
ticket_link: 'https://www.greenvelope.com/event/GrandBazaar'
show_sponsors: true
video_thumbnail:
video_url:
beneficiary: inSTEM
beneficiary_description_html: >-
  <p><a data-cms-editor-link-style="undefined" target="_blank"
  href="http://www.instematdpu.com/">inSTEM</a>, offered through DePaul
  University's College of Education, began as a tuition-free, one week, half-day
  camp in the summer of 2014. The program&rsquo;s goal is to inspire and support
  middle-school girls from underserved communities in pursuing their innate
  interest in STEM learning. The camp provides an encouraging environment that
  helps prepare these girls for the challenges of their future math and science
  curriculum, and nurtures their love of STEM. Now a three-week, full-day summer
  camp, the program also includes Mentorship Programs for high school and
  college women, Fall Enrichment Saturdays, local Robotics Competitions and
  National Flagway Mathematics Tournaments.</p>
beneficiary_images:
  - /img/social-responsibility/Blinderman-instem-3.jpg
  - /img/social-responsibility/Blinderman-instem-1.jpg
  - /img/social-responsibility/Blinderman-instem-2.jpg
tier_sponsors:
  - tier_name: Diamond Sponsors
    tier_description:
    tier_order: '1'
    sponsors:
      - sponsor_name: Aon
        sponsor_logo_image: /uploads/aon-small-1.png
        sponsor_url: 'https://www.aon.com/home/index.html'
      - sponsor_name: Azteca
        sponsor_logo_image: /uploads/aztecafoods-small-1.png
        sponsor_url: 'http://www.aztecafoods.com/'
      - sponsor_name: 'Talent Hitch, Inc.'
        sponsor_logo_image: /uploads/talent-hitch-logo-1.png
        sponsor_url: 'https://talenthitch.careers/'
      - sponsor_name: Wintrust Bank
        sponsor_logo_image: /uploads/wintrust.jpg
        sponsor_url: 'https://www.wintrust.com/'
  - tier_name: Sapphire Sponsors
    tier_description:
    tier_order: '2'
    sponsors:
      - sponsor_name: ALL Masonry
        sponsor_logo_image: /uploads/all-masonry-logo-2.jpg
        sponsor_url: 'https://allmasonry.com/'
      - sponsor_name: Clark Construction
        sponsor_logo_image: /uploads/clark-chicago-logo.jpg
        sponsor_url: 'https://www.clarkconstruction.com/'
      - sponsor_name: Continental Painting
        sponsor_logo_image: /uploads/continental.jpg
        sponsor_url: 'https://continentalpainting.com/'
      - sponsor_name: FGM Architects
        sponsor_logo_image: /uploads/fgm-new-2.png
        sponsor_url: 'http://www.fgmarchitects.com/'
      - sponsor_name: JADE Carpentry
        sponsor_logo_image: /uploads/jade-small-1.png
        sponsor_url: 'https://www.jadecarpentry.com/'
      - sponsor_name: Liberty Mutual
        sponsor_logo_image: /uploads/lmac-v-surety-rgb-2-color-1.png
        sponsor_url:
      - sponsor_name: Simpson Gumpertz & Heger
        sponsor_logo_image: /uploads/sgh-1.png
        sponsor_url: 'https://www.sgh.com/'
  - tier_name: Ruby Sponsors
    tier_description:
    tier_order: '3'
    sponsors:
      - sponsor_name: Ameresco
        sponsor_logo_image: /uploads/am-notag-pms1-1.png
        sponsor_url:
      - sponsor_name: d'Escoto
        sponsor_logo_image: /uploads/descoto-2.png
        sponsor_url: 'http://www.descotoinc.com/'
      - sponsor_name: Fetch IMC
        sponsor_logo_image: /uploads/fetch-full-logo-black-thick-medium-1.png
        sponsor_url: 'https://fetchimc.com/'
      - sponsor_name: Larson Storm Doors and Windows
        sponsor_logo_image: /uploads/larsonlogo-goldvector.png
        sponsor_url: 'https://www.larsondoors.com/'
      - sponsor_name: Matrix Capital
        sponsor_logo_image: /uploads/matrix-2.png
        sponsor_url: 'https://matrixcapital.com/'
      - sponsor_name: Plante Moran
        sponsor_logo_image: /uploads/plantemoran-2.png
        sponsor_url: 'https://www.plantemoran.com/'
      - sponsor_name: Primera Engineers
        sponsor_logo_image: /uploads/primera-logo.jpg
        sponsor_url: 'http://primeraeng.com/'
      - sponsor_name: Sterling Bay
        sponsor_logo_image: /uploads/sb-type-helvetica-2.png
        sponsor_url: 'https://www.sterlingbay.com/'
      - sponsor_name: Technisource Group
        sponsor_logo_image: /uploads/triple-rings-logo-all-black.png
        sponsor_url:
  - tier_name: Emerald Sponsors
    tier_description:
    tier_order: '4'
    sponsors:
      - sponsor_name: ABS Electrical
        sponsor_logo_image:
        sponsor_url:
      - sponsor_name: Amber Mechanical Contractors
        sponsor_logo_image:
        sponsor_url: 'https://amberheatingandair.com/'
      - sponsor_name: Bauer Latoza Studio
        sponsor_logo_image:
        sponsor_url:
      - sponsor_name: Cotter Consulting
        sponsor_logo_image:
        sponsor_url: 'https://www.cotterconsulting.com/'
      - sponsor_name: Klein & Hoffman
        sponsor_logo_image:
        sponsor_url: 'http://www.kleinandhoffman.com/'
      - sponsor_name: Martin Peterson
        sponsor_logo_image:
        sponsor_url: 'http://www.mpcmech.com/'
      - sponsor_name: Mayer and Marsh
        sponsor_logo_image:
        sponsor_url:
      - sponsor_name: Project Management Advisors
        sponsor_logo_image:
        sponsor_url: 'https://www.pmainc.com/'
      - sponsor_name: Midwest Fence
        sponsor_logo_image:
        sponsor_url:
      - sponsor_name: The Mom Project
        sponsor_logo_image:
        sponsor_url: 'https://themomproject.com/'
      - sponsor_name: 'Homiey, Coworking Space'
        sponsor_logo_image:
        sponsor_url: 'https://www.homiey.com/'
      - sponsor_name: Hayes Mechanical
        sponsor_logo_image:
        sponsor_url:
carousel_images:
  - image:
---

**A grand celebration for a great cause\!**

**We're proud to announce the Grand Bazaar, our 4th annual fundraiser in support of&nbsp;[inSTEM](http://www.instematdpu.com/){: target="_blank"}.**

5:00 p.m. – VIP Pre-Party<br>5:30 p.m. – Grand Bazaar

Join us on Thursday, October 24 to celebrate a wonderful cause with delicious Mediterranean food and drinks. With the support of our colleagues, family and friends, Blinderman’s annual fundraisers have raised nearly $150,000 for inSTEM.&nbsp;

Our core purpose is to be the best and make an impact. We hope you’ll join us in making this year’s fundraiser our most successful yet.