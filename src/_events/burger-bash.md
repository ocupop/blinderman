---
layout: event
title: Burger Bash
headline:
event_date: 2017-10-19 00:00:00
event_date_final: true
start_time: '4:30 pm'
end_time: '7:30 pm'
sponsorship_start_date: 2017-10-19 00:00:00
sponsorship_deadline_date: 2017-10-19 00:00:00
purchase_tickets_start_date: 2017-10-19 00:00:00
location_name: Blinderman Construction
address: '224 N. Desplaines St., Suite 650, Chicago, IL 60661'
poster_image: /uploads/burgerbash-stamp.jpg
ticket_link:
show_sponsors: false
video_thumbnail:
video_url:
beneficiary:
beneficiary_description_html:
beneficiary_images:
  -
tier_sponsors:
  - tier_name:
    tier_description:
    tier_order:
    sponsors:
      - sponsor_name:
        sponsor_logo_image:
        sponsor_url:
carousel_images:
  - image:
---

