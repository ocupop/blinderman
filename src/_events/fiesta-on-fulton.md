---
layout: event
headline: Blinderman's 3rd Annual inSTEM Fundraiser
title: Fiesta on Fulton
event_date: 2018-11-08 00:00:00
event_date_final: true
start_time: '4:30 pm'
end_time: '7:30 pm'
sponsorship_start_date: 2018-05-31 00:00:00
sponsorship_deadline_date: 2018-09-17 00:00:00
purchase_tickets_start_date: 2018-09-26 00:00:00
location_name: Carnivale
address: '702 W. Fulton St., Chicago, IL'
poster_image: /uploads/fiesta-banner.png
ticket_link:
show_sponsors: true
video_thumbnail:
video_url:
beneficiary: inSTEM
beneficiary_description_html: >-
  <p><a data-cms-editor-link-style="undefined" target="_blank"
  href="http://www.instematdpu.com/">inSTEM</a> (Inspiring STEM in Girls) is a
  summer camp program, offered through DePaul University's College of Education,
  that builds on the fundamentals of the Young People&rsquo;s Project (YPP) for
  mathematics literacy. The purpose of the inSTEM program is to provide middle
  school girls, in low-income <a data-cms-editor-link-style="undefined"
  target="_blank" href="http://cps.edu/Pages/home.aspx">Chicago Public
  Schools</a> (CPS), unique opportunities to engage in STEM activities. These
  experiences include real-world investigations, problem solving, and technology
  integrations, all designed to enhance students&rsquo; mathematics and science
  content knowledge and promote students&rsquo; interest and confidence in
  advanced education and STEM careers.</p>
beneficiary_images:
  - /img/social-responsibility/Blinderman-instem-3.jpg
  - /img/social-responsibility/Blinderman-instem-1.jpg
  - /img/social-responsibility/Blinderman-instem-2.jpg
tier_sponsors:
  - tier_name: Premier
    tier_description:
    tier_order: '1'
    sponsors:
      - sponsor_name: Aon
        sponsor_logo_image: /uploads/aon-small.png
        sponsor_url: 'http://www.aon.com/'
      - sponsor_name: Talent Hitch
        sponsor_logo_image: /uploads/talenthitch-logo-small.png
        sponsor_url: 'https://talenthitch.careers/'
      - sponsor_name: Azteca Foods
        sponsor_logo_image: /uploads/aztecafoods-small1.png
        sponsor_url: 'http://www.aztecafoods.com/'
  - tier_name: Tango
    tier_description:
    tier_order: '2'
    sponsors:
      - sponsor_name: Lendlease
        sponsor_logo_image: /uploads/lendlease.png
        sponsor_url: 'https://www.lendlease.com/us/'
      - sponsor_name: FGM Architects
        sponsor_logo_image: /uploads/fgm-new-1.png
        sponsor_url: 'https://fgmarchitects.com/'
      - sponsor_name: JADE
        sponsor_logo_image: /uploads/jade-small.png
        sponsor_url: 'https://www.jadecarpentry.com/'
      - sponsor_name: ALL Masonry
        sponsor_logo_image: /uploads/all-masonry-logo-1.jpg
        sponsor_url: 'https://allmasonry.com/'
  - tier_name: Rumba
    tier_description:
    tier_order: '3'
    sponsors:
      - sponsor_name: Clark Construction
        sponsor_logo_image: /uploads/clark-chicago-logo.png
        sponsor_url: 'https://www.clarkconstruction.com/'
      - sponsor_name: Ameresco
        sponsor_logo_image: /uploads/ameresco.png
        sponsor_url: 'https://www.ameresco.com/'
      - sponsor_name: Sterling Bay
        sponsor_logo_image: /uploads/sb-type-helvetica-1.png
        sponsor_url: 'http://www.sterlingbay.com/'
      - sponsor_name: Liberty Mutual
        sponsor_logo_image: /uploads/libertymutual.png
        sponsor_url: 'https://business.libertymutualgroup.com/business-insurance'
      - sponsor_name: Plante Moran
        sponsor_logo_image: /uploads/plantemoran-1.png
        sponsor_url: 'https://www.plantemoran.com/'
      - sponsor_name: Fetch IMC
        sponsor_logo_image: /uploads/fetch-full-logo-black-thick-medium.png
        sponsor_url: 'https://fetchimc.com/'
      - sponsor_name: Continental Painting
        sponsor_logo_image: /uploads/continental.png
        sponsor_url: 'https://continentalpainting.com/'
      - sponsor_name: Matrix Capital Advisors
        sponsor_logo_image: /uploads/matrix-small.png
        sponsor_url: 'http://matrixcapital.com/'
      - sponsor_name: SGH
        sponsor_logo_image: /uploads/sgh-small.png
        sponsor_url: 'http://www.sgh.com/'
      - sponsor_name: Broadway Electric
        sponsor_logo_image: /uploads/bei-logo-blue-1.png
        sponsor_url: 'https://www.broadwayelectric.com/'
      - sponsor_name: Aldridge Electric
        sponsor_logo_image: /uploads/ae-logo-full-copy.png
        sponsor_url: 'https://www.aldridgegroup.com/'
  - tier_name: Cha Cha
    tier_description:
    tier_order: '4'
    sponsors:
      - sponsor_name: ABS Electrical
        sponsor_logo_image:
        sponsor_url:
      - sponsor_name: Cotter Consulting
        sponsor_logo_image:
        sponsor_url:
      - sponsor_name: Klein & Hoffman
        sponsor_logo_image: /uploads/kh.png
        sponsor_url: 'http://www.kleinandhoffman.com/'
      - sponsor_name: Knickerbocker Roofing & Paving Co.
        sponsor_logo_image:
        sponsor_url:
      - sponsor_name: KOO
        sponsor_logo_image:
        sponsor_url: 'http://www.kooarchitecture.com/'
      - sponsor_name: Mayer & Marsh
        sponsor_logo_image:
        sponsor_url: 'http://mayerandmarshlaw.com/'
      - sponsor_name: 'Project Management Advisors, Inc.'
        sponsor_logo_image:
        sponsor_url: 'https://www.pmainc.com/'
      - sponsor_name: Woolpert
        sponsor_logo_image:
        sponsor_url: 'https://woolpert.com/'
carousel_images:
  - image: /uploads/fiestaonfulton-1a.jpg
---

2016 – Food Truck Fest.<br>2017 – Burger Bash.<br><br>**This year the *cervezas* will be cold, and the *salsa* will be hot – let’s Fiesta on Fulton\!**

Blinderman is proud to announce its 3rd annual fundraiser in support of [inSTEM](http://www.instematdpu.com/){: target="_blank"}. Join us to celebrate a wonderful cause with delicious food and drinks. With the support of our colleagues, family and friends, Blinderman's annual fundraiser has raised a collective $64,000 for inSTEM.

Our core purpose is to be the best and make an impact. We hope you’ll join us in making this year’s fundraiser our most successful yet.