---
layout: project
title: New Trier High School Locker Rooms
subtitle: Cohesive project schedules were also designed at this time to include an intricate phasing and implementation plan that was crucial to project success.
intro: Athletic programs have been a key component to a well-rounded educational experience for over 100 years at New Trier High School. Minimizing inconvenience to physical education classes and team sport participation was the prime consideration in planning and executing the long-overdue renovation to the school’s archaic locker room facilities.
expertise_area: education
location: 'Winnetka, IL'
client: New Trier High School District 236
value: 7.2M
scope: Complex Renovation
delivery: Design-Bid-Build
size: '80,000 SF'
video_thumbnail:
video_url:
project_images:
  - project_image: /uploads/NewTrier02.jpg
    caption:
  - project_image: /uploads/NewTrier01.jpg
    caption:
  - project_image: /uploads/NewTrier06.jpg
    caption:
  - project_image: /uploads/NewTrier07.jpg
    caption:
  - project_image: /uploads/NewTrier08.jpg
    caption:
  - project_image: /uploads/NewTrier09.jpg
    caption:
  - project_image: /uploads/NewTrier16.jpg
    caption:
_array_structures:
  award: 
    value:
      url:
      title:
award:
---


The existing locker rooms were added in 1912 to the original campus built in 1901. This project centered on converting a 2-story gymnasium into two, single story locker rooms.

Prior to bidding this project, Blinderman was first engaged by the District to perform preconstruction services to aid in developing comprehensive budget requirements. Cohesive project schedules were also designed at this time to include an intricate phasing and implementation plan that was crucial to project success.

The demolition and renovation combined a high level of complexity with an aggressive construction schedule. With an aim to drastically improve comfort, the new state-of-the-art facilities were enhanced by completely new HVAC, plumbing and electrical systems. The attractive, well-lit spaces included installation of new ceilings, lighting systems, non-slip hi-gloss resin flooring and glazed masonry units – finish materials that are all safe, easily maintained and resilient to excessive use.