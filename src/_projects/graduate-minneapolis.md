---
layout: project
title: Graduate Minneapolis
subtitle: Transforming a common hotel into a Graduate Hotel.
intro: >-
  Blinderman has worked together with AJ Capital Partners to transform
  unremarkable properties in university towns across the country into dynamic
  bespoke hotel experiences.
location: 'Minneapolis, MN'
client: AJ Capital Partners
value:
scope: Complex Renovation
delivery: Design-Build
size: '8-Stories, 304 keys'
video_thumbnail:
video_url:
project_images:
  - project_image: /uploads/graduate-minneapolis-1.jpg
    caption:
  - project_image: /uploads/graduate-minneapolis-2.jpg
    caption:
  - project_image: /uploads/graduate-minneapolis-7.jpg
    caption: Chandeliers were created from vintage snowshoe and fishing flies.
  - project_image: /uploads/graduate-minneapolis-3.jpg
    caption:
  - project_image: /uploads/graduate-minneapolis-4.jpg
    caption:
  - project_image: /uploads/graduate-minneapolis-5.jpg
    caption:
  - project_image: /uploads/graduate-minneapolis-6.jpg
    caption:
  - project_image: /uploads/graduate-minneapolis-8.jpg
    caption:
_array_structures:
  award: 
    value:
      url:
      title:
award:
---

This eight-story, 304-room hotel, located in the heart of the University of Minnesota's East Bank campus, is the only on-campus lodging option. Inspired by Minneapolis’ Norwegian influences and active lifestyle, Graduate Minneapolis welcomes guests with a theme based on a lake house landscape. Awarded Boutique Design New York’s (BDNY) 2018 Gold Key Award for Best Lobby Midscale, Graduate’s comfortable lobby is complete with custom installations including bespoke chandeliers created from vintage snowshoe and fishing flies, an art wall of landscape paintings, and a dramatic fireplace alongside a cozy lounge with a woodland aesthetic.

A compressed schedule required meticulous planning with an operational hotel constantly booked for sporting events, university engagements, and weddings. The project team held daily meetings with the key hotel staff to determine areas of work, guest experiences and turnover schedules. Construction crews met very tight schedules involving room turnover and often structured work sequences around groups booked for specific room types or meeting spaces.

The strong working relationship between our project team and the owner, the hotel’s General Manager and staff, and the subcontractors enable us to tackle challenges together. The work was completed on time for a successful opening on a busy Superbowl weekend.