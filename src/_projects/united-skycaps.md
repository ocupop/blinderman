---
layout: project
title: Terminal 1 Skycaps and Vestibule Restoration
subtitle: A well thought out approach and buttoned-down logistics  are of paramount importance when delivering construction services to the most active terminal at one of the world’s busiest airports.
intro: 'Working landside at O’Hare International Airport carries with it intrinsic and unique challenges: maintaining the construction schedule and budget while protecting the safety of the general public, and minimizing impact on travelers and day-to-day airport operations. These challenges are of prime importance when construction impacts the main terminal entrance/egress points and operating baggage handling functions.'
location: 'O’Hare Airport, IL'
expertise_area: transportation
client: United Airlines
value: 6.2M
scope: Complex Occupied Renovation
delivery: Design-Bid-Build
size: '5 Skycaps & Associated Luggage Conveyors and Entry Vestibules'
video_thumbnail:
video_url:
project_images:
  - project_image: /uploads/t1-vestibule.jpg
    caption:
  - project_image: /uploads/t1-skycapexterior.jpg
    caption:
  - project_image: /uploads/t1-exteriorreno.jpg
    caption:
  - project_image: /uploads/t1-skycappeople.jpg
    caption:
  - project_image: /uploads/t1-inside.jpg
    caption:
  - project_image: /uploads/t1-roof-lift.jpg
    caption:
_array_structures:
  award: 
    value:
      url:
      title:
award:
---


Of the five existing Skycap facilities being refurbished, three were being demolished down to structural steel and completely reconstructed without any disruption to airport activities. Rebuilding a Skycap facility is an extensive procedure that requires substantial protection of the adjacent areas so that in-use sidewalks, roadways, vestibules and terminal are not damaged or obstructed in the process.

With only a windscreened chain link fence separating the jobsite from the operational airport and the general public, the control of dust, fumes, sparks from welding or burning, and other possible dangers is crucial to maintaining a safe environment for all airport personnel and travellers.

Detailed construction management included employment of proactive measures and a creative approach integrating innovative solutions with an exacting phased schedule. Consistent collaboration and communication with airport facilities and operations management personnel was crucial to delivering high-quality, on-time.