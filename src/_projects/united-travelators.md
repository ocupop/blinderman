---
layout: project
title: Travelator Removal
subtitle: "Overcoming challenges landside at O'Hare International Airport."
intro: 'Deceptively simple in concept, these type of phased renovations affecting the main pedestrian traffic flow of the busiest terminal at one of the world’s busiest airports pose many logistical challenges.'
location: O’Hare International Airport
expertise_area: transportation
client: United Airlines
value: 1.8M (Terminal 1-B Project) / $409K (Terminal 1-C Project)
scope: Complex Occupied Renovation
delivery: Design-Bid-Build
size: "T1-B Project – 180' Long Conveyor \nT1-C Project – 500' Long Conveyor"
video_thumbnail: /uploads/screen-shot-2017-06-06-at-4-17-41-pm.png
video_url: 'https://vimeo.com/220828539'
project_images:
  - project_image: /uploads/trav-c.jpg
    caption:
  - project_image: /uploads/travelator5.jpg
    caption:
  - project_image: /uploads/travelator.jpg
    caption:
  - project_image: /uploads/travelator-demo1.jpg
    caption:
_array_structures:
  award: 
    value:
      url:
      title:
award:
---


The scope of work involved demolition of the existing conveyor mechanized walkways and the subsequent restoration to the site with geofoam infill, concrete overlay, and installation of terrazzo flooring finished to match the existing terminal floor finish and pattern.

Protecting the safety of the general public and crew, while minimizing impact on travelers and day-to-day terminal operations, and maintaining project schedule and budget requirements can only be accomplished with intense collaboration, preplanning, and dedicated construction management. The Blinderman team worked together with United to develop detailed plans to managed crucial issues such as waste removal, material delivery and laydown, dust and dirt control, noise control and air quality control to diminish or eliminate any negative consequences to the general public and airport operations.