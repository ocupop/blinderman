---
layout: project
title: The University of Chicago Law School
subtitle: "A complex renovation of one of the country's most prestigious law schools."
intro: 'The University of Chicago Law School project was a complex renovation that touched classrooms, seminar rooms,  auditorium, moot court, locker areas and mechanical rooms. The entire HVAC system was completely renovated, and Blinderman oversaw the installation of state-of-the-art audio, video and data systems.'
location: 'Chicago, IL'
expertise_area: education
client: The University of Chicago
value: 6.8M
scope: Complex Renovation
delivery: Design-Bid-Build
size: '4-Stories, 60,000 SF'
video_thumbnail:
video_url:
project_images:
  - project_image: /uploads/class-9.jpg
    caption:
  - project_image: /uploads/class-5.jpg
    caption:
  - project_image: /uploads/class-6.jpg
    caption:
  - project_image: /uploads/class-10.jpg
    caption:
  - project_image: /uploads/class-21.jpg
    caption:
  - project_image: /uploads/locker-area-23.jpg
    caption:
  - project_image: /uploads/moot-court-8.jpg
    caption:
_array_structures:
  award: 
    value:
      url:
      title:
award:
---


In order to minimize disruption, the majority of&nbsp; this work was put in place during the summer months, when many classes were not in session. A substantial portion of phased demolition and preparation was completed off-hours during the school year to accommodate the quick turnaround of renovation work required to be completed in the summer.

This project required careful planning of jobsite logistics to reduce impact to student and pedestrian traffic in and around the work area due to congestion and space constraints at this always operational university campus.&nbsp; &nbsp;

Blinderman Construction collaborated closely with the University, architect and construction managers, from preconstruction through punch list to ensure the timely and successful completion of this high profile and prestigious facility.