---
layout: project
title: Temple Jeremiah Sanctuary
subtitle: Blinderman met the challenge of having this renovation work completed and new systems in operation in time for the celebration of the High Holy Days.
intro: "As a result of a generous donation, Temple Jeremiah \nwas able to fund  the interior renovation of its sanctuary. The scope of work included the careful installation of many specialty architectural finishes such as a curvilinear wood-clad ceiling system, custom millwork fabricated with exotic anigre veneers, new stone steps leading up to the bimah, and a custom wire mesh wall treatment. New draperies and carpeting were installed and the existing moveable wall  systems were reupholstered."
expertise_area: institutional
location: 'Northfield, IL'
client: Temple Jeremiah
value: 1M
scope: Complex Occupied Renovation
delivery: Design-Bid-Build
size: '9,000 SF'
video_thumbnail:
video_url:
project_images:
  - project_image: /uploads/Temple-1199A_v2.jpg
    caption:
  - project_image: /uploads/Temple-1199B.jpg
    caption:
  - project_image: /uploads/Temple-1199C.jpg
    caption:
  - project_image: /uploads/Temple-1199D.jpg
    caption:
  - project_image: /uploads/Temple-1199E.jpg
    caption:
  - project_image: /uploads/Temple-1199F.jpg
    caption:
_array_structures:
  award: 
    value:
      url:
      title:
award:
---


The main sanctuary lighting system was completely upgraded, including the upgrading the switching patterns for more efficient usage. The existing HVAC systems and fire protection sprinkler system were modified to accommodate the new ceiling systems being installed, and a deluxe audio/visual system was installed to include new flat panel television monitors and speakers.

Blinderman was charged with the task of having the facility renovation work completed and new systems in operation in time for the celebration of the High Holy Days. As all of the construction activities required working around Temple Jeremiah’s schedule for services and events, and due to the fact that the vast majority of the construction work occurred in the bimah and main sanctuary, the Blinderman crew dismantled the construction worksite and installed of temporary protection on a weekly basis in order to have the Sanctuary clean, safe, and ready for regular services and special events such as Bar/Bat Mitzvah services.

As all of the construction activities required working around Temple Jeremiah’s schedule for services and events, and due to the fact that the vast majority of the construction work occurred in the bimah and main sanctuary, the Blinderman crew dismantled the construction worksite and installed of temporary protection on a weekly basis in order to have the Sanctuary clean, safe, and ready for regular services and special events.