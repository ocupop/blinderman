---
layout: project
title: Building 1
subtitle: The integration of new mechanical systems within this occupied facility and the repair and renovation of the building envelope required particularly careful analysis and coordination.
intro: 'Building 1 stands as the most prominent of the original 39 naval station structures constructed in 1911. Designed by Jarvis Hunt and inspired by Italian Renaissance architecture, its design is highlighted by striking ornamentation at the roofline, doorways and windows. Building 1 is listed on the National Register of Historic Places, and our design-build renovation strictly adhered to the Secretary of the Interior Standards for Rehabilitation.'
expertise_area: government
location: 'Naval Station Great Lakes, IL'
client: U.S. Navy
value: 18.2M
scope: Historic Complex Occupied Renovation
delivery: Design-Build
size: '85,000 SF'
video_thumbnail:
video_url:
project_images:
  - project_image: /uploads/GLNB_10.08_01.jpg
    caption:
  - project_image: /uploads/Bldg1_aerial.jpg
    caption:
  - project_image: /uploads/GLNB_10.08_15.jpg
    caption:
  - project_image: /uploads/GLNB_10.08_19.jpg
    caption:
  - project_image: /uploads/GLNB_10.08_17.jpg
    caption:
  - project_image: /uploads/GLNB_10.08_27.jpg
    caption:
  - project_image: /uploads/Bldg1_smscaffold.jpg
    caption:
_array_structures:
  award: 
    value:
      url:
      title:
award:
---


The design-build team was responsible for integrating new HVAC/MEP systems and repairing deteriorated building envelope and interiors, while maintaining the building’s historical status. Runs of duct work were placed on the roof to minimize the impact to the interior of the building.
<br>Smooth and ornate plaster wall, moulding and ceiling areas that were opened up to facilitate the installation of equipment were repaired and restored to match original condition.

All renovations needed to be completed in phases to keep the building in operation during construction. Building 1 contains the offices for most senior officials at Great Lakes. Our team devised an intricate plan to phase systems installation to minimize both the need to relocate personnel, and interfere with ongoing building operations.

This project has earned and Outstanding performance rating, exceeding all Navy expectations.