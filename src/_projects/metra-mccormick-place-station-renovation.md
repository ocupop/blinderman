---
layout: project
title: Metra McCormick Place Station Renovation
subtitle: >-
  Upgrades to the Metra station at McCormick Place included a vibrant series of
  mural paintings from Chicago Public School students who participate in the
  After School Matters program.
intro: >-
  When a major trade show or convention is at McCormick Place, the Metra
  Electric Line serves thousands of additional customers per day.
location: 'Chicago, IL'
client: Metra
value: $1.45M
scope: Complex Renovation
delivery: Design-Build
size:
video_thumbnail:
video_url:
project_images:
  - project_image:
    caption:
_array_structures:
  award: 
    value:
      url:
      title:
award:
---

The Metra Electric Line train station serves the McCormick Place Convention Center; it receives thousands of additional customers per day during a major trade show or convention.

Metra and the Metropolitan Pier and Exposition Authority (MPEA) planned improvements to the station which Blinderman's team delivered.&nbsp;

Blinderman's team coordinated the installation of digital signage and a sound system to allow customers to hear train announcements without having to travel to the platform area below.

Renovation of the McCormick Place Station included new suspended architectural ceiling over the stairway and platform level, wall and column surfaces, storefront glazing, doors, windbreak enclosures with on demand heaters, benches, ADA guardrails and handrails, small size concrete stairs, platform and stair surface upgrade.&nbsp;

Renovation also includes new illuminated art frames to be installed on crash walls located outside the platform level on both sides of the tracks, lighting and signage upgrades. Lighting upgrade includes new LED lighting over the stairway and partial platform lighting, both incorporated into the suspended ceilings.

The project’s greatest challenge was coordinating wall panels and ceiling system changes throughout the project. In the permitting process with the city, the project architect was required to change the type of wall panels and the definition of the space. These changes created logistical challenges for how the wall panels could be hung, which panels could be used, and where these panels could be obtained. The change to the definition of the space along with the discovered condition of a beam not being located where shown on the drawings, required multiple revisions to the ceiling system while in design and then while being installed.

Working closely with Metra to schedule flagging duties weekly to meet project dates and include all project activities was an additional difficult challenge.

Blinderman provided smart solutions to the structural issue, by bringing on a Structural Engineer and working hand in hand with 555 international to revise their designed ceiling system during installation. Blinderman also coordinated their workers to never have a time where Metra showed up for flagging duties and no work was going to be performed.