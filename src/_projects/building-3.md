---
layout: project
title: Building 3
subtitle: Our value-added solution design-build solution maximized previously usable space to enhance functionality for building occupants.
intro: Building 3 was originally constructed in 1911 and is a property of major significance within the historic district of the naval base. The building houses Training Support Center Great Lakes and the base chapel/chaplain offices.
expertise_area: government
location: 'Great Lakes, IL'
client: U.S. Navy
value: 17M
scope: Historic Complex Occupied Renovation
delivery: Design-Build
size: '3-stories, 85,000 SF'
video_thumbnail:
video_url:
project_images:
  - project_image: /uploads/Building-03_29.jpg
    caption:
  - project_image: /uploads/Building-03_20.jpg
    caption:
  - project_image: /uploads/Building-03_30.jpg
    caption:
  - project_image: /uploads/Building-03_33.jpg
    caption:
  - project_image: /uploads/Building-03_34.jpg
    caption:
  - project_image: /uploads/Building-03_8167.jpg
    caption:
_array_structures:
  award: 
    value:
      url:
      title:
award:
---


Building 3 boasts interior spaces deemed to have significant historic character defining elements. All renovation and restoration efforts followed the Secretary of the Interior Standards for Rehabilitation to achieve SHPO/IHPA concurrence that the work had no adverse effect on the structure.

With a thorough understanding of both the exterior envelope and interior space renovation program requirements, the design-build team developed a solution responsive to function and value, with little impact to the existing historic character of the building. Historic preservation oversight incorporated into the design phase ensured accurate and compatible solutions for all affected interior finishes and building envelope improvements.

The design-build team developed an innovative holistic solution to the challenge of providing new mechanical/electrical and conveyor systems with minimum impact to original profile of the historic structure. A significant challenge was posed by the design and installation of an ADA-compliant elevator in a facility that previously was only accessed by stairs.

Our team’s solution to design and install supplemental interior AT/FP (Anti-Terrorism/Force-Protection) windows inside existing refurbished wood windows (in lieu of full replacement windows) achieved the required thermal performance and also maintained compliance with AT/FP and historic preservation requirements. A total of 38 &nbsp;monumental windows were restored. The refurbished windows required an exact re-installation to meet the tight tolerance levels necessary to meet the blast-resistant requirements of the government’s stringent AT/FP standards.