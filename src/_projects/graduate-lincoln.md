---
layout: project
title: Graduate Lincoln
subtitle: A carefully curated and skillfully crafted heartland retreat.
intro: >-
  This Graduate brand hotel was designed and hand-crafted to reflect the Husker
  school spirit and the corn-country vibe of the nation’s heartland.
location: 'Lincoln, NE'
expertise_area: hospitality
client: AJ Capital Partners
value:
scope: Complex Renovation
delivery: CM at Risk
size: '16-Stories, 234 Keys'
video_thumbnail:
video_url:
project_images:
  - project_image: /uploads/graduatelincoln-lobby.jpg
    caption:
  - project_image: /uploads/graduatelincoln-restaurant-1.jpg
    caption:
  - project_image: /uploads/graduatelincoln-room.jpg
    caption:
  - project_image: /uploads/graduatelincoln-ballroom.jpg
    caption:
  - project_image: /uploads/graduatelincoln-pool-1.jpg
    caption:
  - project_image: /uploads/graduatelincoln-exterior-1.jpg
    caption:
_array_structures:
  award: 
    value:
      url:
      title:
award:
---

Blinderman managed guestroom and bathroom renovations in 234 rooms including new floor finishes, wall finishes and furniture/fixture/equipment (FFE) installation.

Custom design features inspired by and tailored to the local college or university is the basis of the Graduate brand. The restaurant and bar, lobby, pool, ballrooms and meeting rooms were all rebranded and refurbished from floor to ceiling to reinforce the brand image, and implement general code corrections.

The 16-story, 234-room hotel’s redesigned lobby features herringbone wood floors, brick walls, forest green tile and old-school arcade games. Vintage TV sets serve as nightstands in the guest rooms and Johnny Carson Show inspired drapery at the front desk (a nod to the late-night host and native son who attended the University of Nebraska-Lincoln).

The Blinderman team worked hand-in-hand with AJ Capital designers from the onset of the design process to craft the construction solutions that faithfully render their unique vision in keeping with the high standard of quality the Graduate brand demands.