---
layout: project
title: Hotel Philips
subtitle: 21st century comfort and convenience integrated with classic elegance of a bygone era.
intro: "The Hilton Curio brand is a curated collection of unique\_properties that appeal to travelers seeking a truly local\_experience."
location: 'Kansas City, MO'
expertise_area: hospitality
client: Arbor Lodging Partners
value:
scope: Historic Occupied Renovation/Curio Brand Conversion
delivery: CM at Risk
size: '37,000 SF'
video_thumbnail:
video_url:
project_images:
  - project_image: /uploads/Hotel-Phillips_01.17.17_027.jpg
    caption:
  - project_image: /uploads/Hotel-Phillips_01.17.17_016.jpg
    caption:
  - project_image: /uploads/Hotel-Phillips_01.17.17_033.jpg
    caption:
  - project_image: /uploads/Hotel-Phillips_01.17.17_026.jpg
    caption:
  - project_image: /uploads/Hotel-Phillips_01.17.17_036.jpg
    caption:
  - project_image: /uploads/Hotel-Phillips_01.17.17_037.jpg
    caption:
  - project_image: /uploads/Hotel-Phillips_Bar-at-Tavernonna.jpg
    caption:
  - project_image: /uploads/Hotel-Phillips_Main-Entrance1_Hotel-Phillips-Photo-Credit.jpg
    caption:
_array_structures:
  award: 
    value:
      url:
      title:
award:
---


Hotel Phillips is listed on the register of Historic Places, and Blinderman Construction partnered with Arbor Lodging Partners to bring about the brand conversion with a careful renovation that not only restored the beloved hotel to its 1930’s Art Deco elegance, but also provided the modern comfort and convenience desired by its 21st century patrons.

Infrastructure improvements included general upgrades to HVAC, electrical and communication systems. Fancoil units were replaced in 153 guest rooms. Building envelope repairs involved lintel replacement and repair to façade historic ornamental terracotta.

The hotel main entry and lobby were reconfigured to allow for a smoother transition intake process, and Blinderman also completed construction to accommodate 3 new elevated food and beverage concepts :

* Tavernonna farm-to-table restaurant/dining area
* P.S. Speakeasy cocktail lounge & banquet room
* Kilo Charlie Coffee Cafe

Strict adherence to a precise and phased schedule was required to keep the hotel in operation throughout completion of this comprehensive scope that touched all areas of the historic Hotel Phillips.