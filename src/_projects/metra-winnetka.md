---
layout: project
title: Winnetka Metra Train Station
subtitle: 'Faced with a minimal construction cost difference and a significant decrease in future maintenance costs, the Village of Winnetka readily adopted our value-engineered solution.'
intro: Exacting scheduling and project team coordination was a crucial component to completing this challenging renovation in a fully operational mass transit facility.
location: 'Winnetka, IL'
expertise_area: transportation
client: Metra
value: 5.2M
scope: Complex Occupied Renovation
delivery: Design-Bid-Build
size:
video_thumbnail:
video_url:
project_images:
  - project_image: /uploads/winnmetra-02.jpg
    caption:
  - project_image: /uploads/winnmetra-07.jpg
    caption:
  - project_image: /uploads/winnmetra-11.jpg
    caption:
  - project_image: /uploads/winnmetra-05.jpg
    caption:
  - project_image: /uploads/20100519-09.JPG
    caption:
_array_structures:
  award: 
    value:
      url:
      title:
award:
---


It seemed like a standard hard bid project for Metra to renovate the existing pedestrian bridge along with the associated platforms and facilities at the Winnetka train station. We submitted our estimate based on Metra’s specifications and drawings, and were awarded the project. Business as usual, you would think, but there was something bothering us – something we just simply could not ignore.

Our preconstruction experts recognized that the bridge renovation as designed was a subpar solution. Not only was there a better way, there was a MUCH safer, more sustainable and efficient solution involving the design and installation of a premanufactured truss bridge. At no cost to Metra, Blinderman presented this re-engineered solution with complete benefits and cost analysis.

Faced with a minimal construction cost difference and a significant decrease in future maintenance costs, the Village of Winnetka readily adopted our value-engineered solution. The project was completed with little inconvenience to Winnetka’s commuters and zero interruption to train service.

At the completion of this successful project, Metra named Blinderman Construction “Contractor of the Year”.