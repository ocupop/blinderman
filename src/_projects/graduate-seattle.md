---
layout: project
title: Graduate Seattle
subtitle: Seismic Renovations and Mount Rainer Rooftop Views
intro:
location: 'Seattle, WA'
expertise_area: hospitality
client: AJ Capital Partners
value:
scope: 'Historic, Complex Phased Renovation'
delivery: CM at Risk
size: '16 Stories, 154 Keys'
video_thumbnail:
video_url:
project_images:
  - project_image: /uploads/seattleweb-1.png
    caption:
  - project_image: /uploads/seattleweb-3.png
    caption:
_array_structures:
  award: 
    value:
      url:
      title:
award:
---

The original Edmond Meany Hotel hotel first opened in 1931 and its construction was groundbreaking: this building was one of the first in the region constructed of poured concrete rather than brick and terra cotta. The unique shape was designed to give every guest room a corner view. The hotel received a makeover in the late 1990s when it became the Meany Tower Hotel, and then eventually Hotel Deca. When the property purchased by AJ Capital Partners, Blinderman was called upon to do what we do best: complex historic renovation.

Our team worked diligently to preserve original features, predominantly in guest-rooms, and a vast marble lobby with original Terrazzo floor. Graduate Seattle’s renovation required significant structural modifications to execute a complicated seismic retrofit mandated by code. The comprehensive scope included renovation to guest-rooms, common areas, event spaces, and amenities spaces. The scope included the demolition and repairs required for the structural upgrades, as well as all new finishes affecting almost the entire existing building. The 16th floor guest-room area was converted to create a rooftop cocktail lounge amenity with indoor and outdoor seating.

Graduate Seattle’s new rooftop bar, the glass-sided Mountaineering Club, delivers panoramic views of Mount Rainier and the Olympic range including a wraparound viewing deck. The Graduate brand design aesthetic includes many custom elements to tie the experience to the location which necessitate ex&shy;treme attention to detail, and the rooftop bar was a perfect location to showcase a unique piece of artwork. Blinderman secured a contractor specializing in custom wood carving to fabricate custom wood panels carved with pacific northwest landscapes to be installed on the bar face. This local craftsman was also contracted to created and install millwork at the new registration desk in the hotel lobby.