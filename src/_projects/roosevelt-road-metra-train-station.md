---
layout: project
title: Roosevelt Road Metra Train Station
subtitle: Replacing a rickety and rusty old  eyesore with a new beaux-arts inspired structure to compliment its Grant Park surroundings and Chicago’s Museum Campus.
intro: 'This challenging, high-profile project required complicated scheduling to coordinate day and night shifts with Metra track closures.'
expertise_area: transportation
location: 'Chicago, IL'
client: Chicago Department of Transportation
value: 10.4M
scope: New Construction – Addition / Renovation
delivery: Design-Bid-Build
size:
video_thumbnail:
video_url:
project_images:
  - project_image: /uploads/RoosvltRd_Metra_24.jpg
    caption:
  - project_image: /uploads/RoosvltRd_Metra_02.jpg
    caption:
  - project_image: /uploads/RoosvltRd_Metra_12.jpg
    caption:
  - project_image: /uploads/RoosvltRd_Metra_13.jpg
    caption:
  - project_image: /uploads/RoosvltRd_Metra_15.jpg
    caption:
  - project_image: /uploads/RoosvltRd_Metra_20.jpg
    caption:
  - project_image: /uploads/RoosvltRd_Metra_27.jpg
    caption:
_array_structures:
  award: 
    value:
      url:
      title:
award:
---


The project scope included rebuilding the existing platforms and adding two new towers with elevators and stairs to each platform to connect to the 11th Street bridge. The reconstruction resulted in a fully accessible and ADA compliant train station. The 300' long train platforms are constructed from a polymer composite material to withstand harsh weather. Fare-vending houses with a canopies and a warming house were installed on the outbound platform.

As most of the construction was completed during non-rush-hour periods to ensure minimal disruption to passengers and train traffic, the station was operational and throughout the construction period.

&nbsp;