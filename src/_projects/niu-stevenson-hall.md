---
layout: project
title: Stevenson Hall Renovation
subtitle: Advanced preplanning and formal partnering fostered the commitment among all stakeholders that ensured success.
intro: 'Northern Illinois University selected Blinderman to complete the demolition and renovation of Stevenson Hall – a residence hall that combines two, 13-story dormitory towers with a central food services area.'
expertise_area: education
location: 'DeKalb, IL'
client: Northern Illinois University
value: 24M
scope: Demolition / Renovation
delivery: Design-Bid-Build
size: '195,000 SF'
video_thumbnail:
video_url:
project_images:
  - project_image: /uploads/STEVENSON-CONNECTING-HALLWAY-5H.jpg
    caption:
  - project_image: /uploads/KITCHEN-10H.jpg
    caption:
  - project_image: /uploads/STEVENSON-CONNECTING-HALLWAY-4H.jpg
    caption:
  - project_image: /uploads/STEVENSON-DINING-AREA-6H.jpg
    caption:
  - project_image: /uploads/STEVENSON-FOOD-COURT-2.jpg
    caption:
  - project_image:
    caption:
  - project_image:
    caption:
  - project_image:
    caption:
_array_structures:
  award: 
    value:
      url:
      title:
award:
---


The project began with asbestos abatement,  and demolition to accommodate the renovation of all dormitory spaces in the two 13-story, 75,0000 SF towers to provide comfortable upgraded residence suites with private ADA compliant bathrooms.

The central building renovation also included construction of new meeting rooms, dining areas and offices areas featuring a curved architectural glass and millwork wall along the building’s main thoroughfare.

The tower renovation required new mechanical and electrical systems, throughout. The central residence hall (approximately 45,000 SF) involved the complete demolition of an existing, outdated kitchen, which was replaced by a food court and associated production kitchen areas.

The central building renovation also included construction of new meeting rooms, dining areas and offices areas featuring a curved architectural glass and millwork wall along the building’s main thoroughfare.