---
layout: project
title: Camp John Paul Jones
subtitle: 'The project began with the development of a 48-acre site and the design/build construction of the first four of what would be seven, 250,000 SF barracks buildings.'
intro: 'Naval Station Great Lakes is the only bootcamp, nationwide, for the training of recruits for the U.S. Navy. Despite the important function served by by this Command, most of the barracks for housing recruits were built between the World War II era and the 1965, and had outlived their usefulness in terms of expandability, materials and environmental conditions.'
expertise_area: government
location: 'Great Lakes, IL'
client: U.S. Navy
value: 228.6M
scope: New Construction
delivery: Design-Build In Joint Venture With Clark Construction
size: '1,295,000 SF on a 48 acre site'
video_thumbnail:
video_url:
project_images:
  - project_image: /uploads/jpj-bridge-7.jpg
    caption:
  - project_image: /uploads/jpj-barrackentrance01.jpg
    caption:
  - project_image: /uploads/jpj-campjpj-aerial.jpg
    caption:
  - project_image: /uploads/jpj-barracks-entry-1.jpg
    caption:
  - project_image: /uploads/jpj-close-up-of-outside-barrack.jpg
    caption:
  - project_image: /uploads/jpj-cp-barracknplaza01.jpg
    caption:
_array_structures:
  award: 
    value:
      url:
      title:
award:
---


Each of the barracks buildings has berthing areas for 1,056 recruits, state-of-the-art classrooms and a full service galley and dining facility. Extensive camp infrastructure improvements included construction of a 750,000 gallon elevated water tank and a double-track, three span railroad bridge and underpass.

AT/FP (Anti-Force/Anti-Terrorism) compliant features were incorporated in the camp design with respect to gate structure, and building locations, glazing and landscape features.

The barracks are laid out in a rectangle to better resemble a university quadrangle. The building design features red brick masonry and sloping roofs to honor the aesthetic tradition of the historic area of the Great Lakes facility dating back to 1911. The building designs evoke nautical themes based on the heritage of naval construction. Mimicking the exposed construction found in marine architecture, the exposed metal decks and concrete support the “Quarterdeck of the Navy” theme established at the Recruit Training Command.