---
layout: project
title: 'Chicago Public Schools Improvement Program, Package 2'
subtitle: 'The Blinderman team successfully designed, engineered and safely constructed over $14M in renovations at the three scattered sites over a single 6-week period.'
intro: 'The 2013 School Investment Program included renovations to 98 Chicago Public School facilities. The Blinderman design-build team was awarded Package #2, which required the design-build renovation of three public school facilities – Lincoln Park Highschool, Marshall Middle School, and Northwest Middle School – all to be completed under an aggressive, phased schedule.'
expertise_area: education
location: '3 sites in Chicago: Lincoln Park High School, Marshall Middle School, NOrthwest Middle School'
client: Chicago Public Building Commission
value: 14M
scope: Complex Renovation
delivery: Design-Build
size:
video_thumbnail:
video_url:
project_images:
  - project_image: /uploads/Lincoln_Park_High_School.jpg
    caption:
  - project_image: /uploads/LincolnPark_05.jpg
    caption:
  - project_image: /uploads/LincolnPark_08.jpg
    caption:
  - project_image: /uploads/Marshall_12.jpg
    caption:
  - project_image: /uploads/Marshall_16.jpg
    caption:
  - project_image: /uploads/Marshall_19.jpg
    caption:
  - project_image: /uploads/NorthWest_02.jpg
    caption:
  - project_image: /uploads/NorthWest_10.jpg
    caption:
_array_structures:
  award: 
    value:
      url:
      title:
award:
---


The Blinderman team successfully designed, engineered and safely constructed over $14M in renovations at the three scattered sites over a single 6-week period – exceeding a significant minority and woman owned business participation requirements at the prime builder and design lead levels, and owner quality expectations.

Varying scopes to add or upgrade a range of amenities at the schools included:

* Boiler and mechanical system repairs
* Building envelope and roof repairs
* Air conditioning classrooms along with necessary electrical upgrades
* Accessibility upgrades for code and program-specific clusters, including vertical transportation (elevators, lifts, etc.
* Wired and wireless technology upgrades
* New or enhanced food service, media and computer, science laboratories and libraries
* General renovations, including lighting, ceilings, painting, and finishes