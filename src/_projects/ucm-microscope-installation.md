---
layout: project
title: Microscope Installation
subtitle: 'Making an impact by minimizing the impact, this project within the Duchossois Center for Advanced Medicine outpatient facility was completed over a single weekend.'
intro: Adherence to a detailed schedule and logistics plan was critical to meeting the tight timeline.
location: 'Chicago, IL'
expertise_area: healthcare
client: University of Chicago Medical Center
value: 70K
scope: Complex Occupied Renovation
delivery: Design-Bid-Build
size: 500 SF
video_thumbnail:
video_url:
project_images:
  - project_image: /uploads/imag0671.jpg
    caption:
  - project_image: /uploads/dcam-microscope.jpg
    caption:
_array_structures:
  award: 
    value:
      url:
      title:
award:
---


UCM continually upgrades their equipment to stay current with cutting edge technology. This project involved working within the existing and operational surgical floor to install the structural support and infrastructure for the a new surgical microscope in OR 429.

The project scope included structural connections from the ceiling deck to support the new microscope mount. Ceiling and wall demolition was required to accommodate power and video conduit and cabling runs.

This fast-track technical project was completed over a single weekend to minimize impact to the existing outpatient services. As the process included removal and replacement of the existing ceiling to allow the installation, robust infection control measures were required to protect the health and safety of the UCM patients at all times.