---
layout: project
title: West Loop Library
subtitle: >-
  Blinderman Construction collaborated with developer Sterling Bay to deliver
  this public library and community center to the local residents.
intro: >-
  Blinderman Construction collaborated with developer Sterling Bay and renowned
  architectural firm Skidmore Owings & Merrill to deliver this much needed
  public library and community center to the residents of the transformed West
  Loop neighborhood.
location: '118 N. Aberdeen, Chicago, IL'
expertise_area: institutional
client: Sterling Bay
value: 1.8M
scope: Adaptive Reuse / Complex Renovation
delivery: CM at Risk
size: '2-Stories, 16,500 SF'
video_thumbnail:
video_url:
project_images:
  - project_image: /uploads/west-loop-branch-3.jpg
    caption:
  - project_image: /uploads/west-loop-branch2.jpg
    caption:
  - project_image: /uploads/west-loop-branch3.jpg
    caption:
  - project_image: /uploads/west-loop-branch4.jpg
    caption:
  - project_image: /uploads/west-loop-branch5.jpg
    caption:
  - project_image: /uploads/west-loop-branch6.jpg
    caption:
_array_structures:
  award: 
    value:
      url:
      title:
award: 
  - url: https://chicago.curbed.com/2019/11/25/20981912/west-loop-library-architecture-award-skidmore-owings-merrill
    title: AIA Illinois - Frank Lloyd Wright Award
  - url: https://aiachicago.awardsplatform.com/gallery/emYNOepg/poNMvLlX?search=044a0a79ae93f36d-119
    title: AIA Chicago Awards 2019 - Divine Detail Award
---

As with much of the redevelopment in the area, this manufacturing/warehouse building – previously a Harpo Studios production facility – has been given new purpose and a modern look.

In keeping with the West Loop’s intrinsic industrial character, non-structural walls were removed, structural masonry walls were opened, and the original bow-truss ceiling and skylights were exposed to allow the creation of lofty interior spaces including a “tinkering” lab, story-telling alcoves, learning/study spaces, a sound studio, and a community meeting room.

Adaptive reuse projects present unique construction challenges requiring out-of-the-box thinking, and the [West Loop Library](https://www.chipublib.org/locations/88/){: target="_blank"} was no exception to the rule. Unforeseen issues invariably present when reconfiguring and integrating mechanical and electrical systems, and care must be given to be responsive to code requirements, building function and aesthetics without impacting the schedule. Our team worked through several options before finding the best method for “pre-weathering” the specified corten steel façade components to ensure a uniform appearance while meeting the design intent.

Binderman’s significant expertise in complex renovation was critical in the process of providing the West Loop community with this wonderful place to gather, think, read, tinker and grow.