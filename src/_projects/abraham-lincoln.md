---
layout: project
title: Abraham Lincoln Elementary
subtitle: 'Working in a congested residential Chicago neighborhood, on a tight site, in a politically charged environment, contributed greatly to logistical and scheduling challenges with traffic control, receiving deliveries, establishing work hours, and locating material lay down areas.'
intro: >-
  To minimize disruption to the community, Blinderman began with intensive
  preplanning to develop and implement highly detailed plans coordinating
  deliveries of construction materials, material lay down areas, and schedule

  work hours.
expertise_area: education
location: 'Chicago, IL'
client: Chicago Public Building Commission
value: 14.1 M
scope: New Construction – Addition / Renovation
delivery: Design-Bid-Build
size: '3-Stories,  40,000 SF'
video_thumbnail:
video_url:
project_images:
  - project_image: /uploads/lincoln-10-02-15-001.jpg
    caption:
  - project_image: /uploads/dsc-0006-mv-ywhxrfvkwwav.JPG
    caption:
  - project_image: /uploads/lincoln-10-02-15-012.jpg
    caption:
  - project_image: /uploads/Lincoln_10.02.15_018.jpg
    caption:
  - project_image: /uploads/lincoln-10-02-15-019.jpg
    caption:
  - project_image: /uploads/lincoln-10-02-15-021.jpg
    caption:
  - project_image: /uploads/lincoln-10-02-15-023.jpg
    caption:
  - project_image: /uploads/lincolnpark-05.jpg
    caption:
  - project_image: /uploads/marshall-12.jpg
    caption:
  - project_image: /uploads/marshall-19.jpg
    caption:
_array_structures:
  award: 
    value:
      url:
      title:
award:
---


This masonry addition to an existing school building adds a new kitchen and lunchroom, nineteen classrooms, two music rehearsal rooms, a spacious rooftop play area, and an exterior playground to what was previously a severely overcrowded Chicago public school. Renovations to the existing circa 1894 school building were also included in the scope, including a new elevator, a vertical lift, a new fire alarm system, and interior renovation to classrooms.

This project’s inherent challenges included an aggressive construction schedule that called for erecting the building structure in the middle of one of the coldest winters on record. In addition to over one month lost to bad weather days, the project encountered several delays due to public utility coordination and discovered site conditions. Despite these delays, the facility was completed as scheduled in time for school to open.