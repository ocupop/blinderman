---
layout: project
title: Camp Porter
subtitle: 'A world-class, state-of-the art training environment that will prepare America’s Sailors for service in the Fleet for decades to come.'
intro: 'Naval Station Great Lakes is a dynamic environment where training occurs 24 hours a day, seven days a week. Naval Station Great Lakes training  commands and schools proudly graduate thousands of recruits and Sailors every year.'
expertise_area: government
location: 'Naval Station Great Lakes, IL'
client: U.S. Navy
value: 205.1M
scope: New Construction
delivery: Design-Build In Joint Venture With Clark Construction
size: 'Site Area: 145; Acres Barracks: 385,000 SF; Special Barracks: 200,000 SF; Visitor Center: 24,600 SF; Simulated Arms Trainer: 33,400 SF; Consolidated Trainer: 150,000 SF; Parking Garage: 88,000 SF'
video_thumbnail:
video_url:
project_images:
  - project_image: /uploads/Porter_06.10_07.jpg
    caption:
  - project_image: /uploads/Porter_06.10_03.jpg
    caption:
  - project_image: /uploads/Porter-Full-Elevation-with-Recruits.jpg
    caption:
  - project_image: /uploads/Porter-Range-With-Targets-and-Marksmen.jpg
    caption:
  - project_image: /uploads/Porter-Divisible-Classroom.jpg
    caption:
  - project_image: /uploads/Porter_06.10_13.jpg
    caption:
  - project_image: /uploads/Porter_1.jpg
    caption:
  - project_image: /uploads/PorterAerial.jpg
    caption:
_array_structures:
  award: 
    value:
      url:
      title:
award:
---


This project involved developing a master plan for the main naval recruit training campus. An important achievement in the planning and design of this campus is the modernization and unification of three separate campus areas.

Camp Porter’s building exteriors reflect the design attributes of the historic brick buildings located elsewhere on the Great Lakes campus. The exterior fa&ccedil;ade design and material selection complements the existing barracks buildings on base. To better acclimate recruits to Navy life, barracks are organized with sleeping compartments, dining, quarterdeck and classrooms all under one roof.

The Camp Porter site and building plans were designed to meet all AT/FP (anti-terrorism/force protection) requirements. These facilities were also designed and constructed to achieve LEED Silver rating, and to date, the Special Recruit Barracks building has been awarded a LEED Silver Certification by the US Green Building Council.