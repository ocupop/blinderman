---
layout: project
title: 'Operating Room #12 Equipment Installation'
subtitle: A very compressed schedule satisfied the Medical Center’s keen need to bring this operating room on-line.
intro: 'The project resulted in the activation of a previously unused operating room in the University of Chicago’s Center for Care and Discovery. In order to bring the room on-line as an active operating room, the existing ceiling, lighting, and laminar flow HVAC system needed to be removed to install the structural steel necessary to support surgical lighting and other  equipment that is suspended from the ceiling. The scope of work included the installation of solid surface nurse workstations, energy efficient  lighting upgrades, and providing additional cooling to an adjacent IT closet.'
location: 'Chicago, IL'
expertise_area: healthcare
client: University of Chicago Medical Center
value: 160K
scope: Complex Occupied Renovation
delivery: Design-Bid-Build
size: 700 SF
video_thumbnail:
video_url:
project_images:
  - project_image: /uploads/20161215-211533.jpg
    caption:
  - project_image: /uploads/20161215-211448.jpg
    caption:
  - project_image: /uploads/20161215-211556.jpg
    caption:
  - project_image: /uploads/20161215-211438.jpg
    caption:
  - project_image: /uploads/20161215-211545.jpg
    caption:
  - project_image: /uploads/ucm-or12-1.jpg
    caption:
  - project_image: /uploads/20161215-200829.jpg
    caption:
  - project_image: /uploads/or-12.jpg
    caption:
_array_structures:
  award: 
    value:
      url:
      title:
award:
---


This construction was put in place on the same floor with 26 other active operating rooms, including 2 active rooms on either side of the site.

Patient safety and hospital operations were of the highest priority to our team. To mitigate risk to patients and disruptions to staff, the entire project was completed off hours. Tight infection control procedures were in place to seal off the construction site and keep it under negative pressure for the duration. All contractor personnel were required to wear protective Tyvek coveralls while moving to and from the operating room. All material, tools, and equipment were cleaned and wrapped in sealed containers and all debris was removed from the site in sealed containers to eliminate the risk of contamination.

Our construction team coordinated with Unistrut (the structural steel subcontractor) and the equipment supplier to develop a design to improve the long term stability of the equipment, and also reduce the amount of demolition required to install the structural steel. This solution shortened the schedule (saving time and money) and proved so successful the University of Chicago Medical Center will continue to apply it as a standard on future operating room activations, and are also considering retrofitting existing operating rooms already in use.