---
layout: project
title: Sauganash Elementary
subtitle: >-
  “They were extremely responsive to any request made by administration and
  problem-solved well if a tough situation arose. There was no disruption of the
  instructional day, in fact our test scores went up! ”

  Christine Munns

  Principal, Sauganash Elementary
intro: 'This addition was sustainably designed with consideration to the use of durable materials, natural light, indoor air quality, water use reduction, and native planting.'
expertise_area: education
location: 'Chicago, IL'
client: Chicago Public Building Commission
value: 10.5M
scope: New Construction – Addition/Renovation
delivery: Design-Bid-Build
size: '2-Stories, 41,000 SF'
video_thumbnail:
video_url:
project_images:
  - project_image: /uploads/Sauganash_10.03.11_01.jpg
    caption:
  - project_image: /uploads/Sauganash_10.03.11_04.jpg
    caption:
  - project_image: /uploads/Sauganash_10.03.11_13.jpg
    caption:
  - project_image: /uploads/Sauganash_10.03.11_08.jpg
    caption:
  - project_image: /uploads/Sauganash_10.03.11_07.jpg
    caption:
  - project_image: /uploads/Sauganash_10.03.11_05.jpg
    caption:
  - project_image: /uploads/Sauganash_10.03.11_02.jpg
    caption:
_array_structures:
  award: 
    value:
      url:
      title:
award:
---


As constructor, Blinderman pursued, optimized and documented construction material use to earn additional Innovation in Design credits. In recognition of Chicago Public Building Commission’s commitment to environmental sustainability in design and construction, the Sauganash Elementary School Addition was awarded the LEED Gold certification.

Construction of this 2-story addition to an existing school included installation of a green roof area, on-site parking for 30 vehicles with underground water detention and permeable pavers. A new playground was constructed with a soft surface walking path that runs the perimeter of the grounds. The renovations required to connect the existing school building to the new addition also increased the school’s accessibility to people with disabilities.

*“We never received a complaint regarding the construction project form our community; in fact, I often received compliments on how respectful Blinderman’ s employees and subcontractors were if they had a question for them.”*
<br>Christine Munns— Principal, Sauganash Elementary