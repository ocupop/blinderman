---
layout: project
title: Graduate Ann Arbor
subtitle: >-
  Boutique bespoke design plus a limited timeframe equals a unique construction
  solution.
intro: >-
  The Graduate Hotels brand concept centers on creating a boutique hotel
  experience in a college town environment. Each Graduate Hotel has a uniquely
  curated and thoughtfully crafted design inspired by its particular school
  community.
expertise_area: hospitality
location: 'Ann Arbor, MI'
client: AJ Capital Partners
value:
scope: Complex Renovation
delivery: CM at Risk
size: '14-Stories, 204 Keys'
video_thumbnail:
video_url:
project_images:
  - project_image: /uploads/Graduate_07.08.16_007.jpg
    caption:
  - project_image: /uploads/Graduate-07.08.16_008.jpg
    caption:
  - project_image: /uploads/Graduate_Main-Lobby.jpg
    caption:
  - project_image: /uploads/Graduate_Stairs.jpg
    caption:
  - project_image: /uploads/Graduate_07.08.16_001.jpg
    caption:
  - project_image: /uploads/Graduate-MainLobby14.jpg
    caption:
  - project_image: /uploads/Graduate-In-Progress.jpg
    caption:
  - project_image: /uploads/Graduate_07.08.16_011.jpg
    caption:
_array_structures:
  award: 
    value:
      url:
      title:
award:
---


Blinderman partnered with the developer, architect and the owner’s representative in December to deliver a top-to-bottom rebranded and renovated facility in Ann Arbor to meet a drop-dead opening date in time for University of Michigan graduation in April.

204 guest room suites were completely renovated including the installation of new windows, high-end floor/wall finishes, plumbing and electrical fixtures, and all new FF&E. Common corridors were also given a face-lift with new finishes, and the Main Lobby and Mezzanine areas were revamped to include a new Reservation Desk and Coffee Bar.

From pulling the project together in preconstruction, to battling through the challenges of working on a fast-track deadline-driven schedule, Blinderman delivered on time and to our customer’s extreme satisfaction. This project’s rousing success has led to additional Graduate project awards now underway in Lincoln NE, Minneapolis MN, Seattle WA, and Iowa City IA.