---
layout: project
title: Beverly Branch Library
subtitle: 'When you build a public library in the city of Chicago, you are also building a community center.'
intro: 'Based on a prototype design, this new single-story full service branch library houses the District Office and Storage Area, has on-site parking for 21 vehicles, a landscaped reading garden and significant improvements to the public way.'
expertise_area: institutional
location: 'Chicago, IL'
client: Chicago Public Building Commission
value: 6.8M
scope: New Construction
delivery: Design-Bid-Build
size: '2-Stories, 16,000 SF'
video_thumbnail:
video_url:
project_images:
  - project_image: /uploads/BevLibrary047.jpg
    caption:
  - project_image: /uploads/BevLibrary003.jpg
    caption:
  - project_image: /uploads/BevLibrary017.jpg
    caption:
  - project_image: /uploads/BevLibrary012.jpg
    caption:
  - project_image: /uploads/BevLibrary004.jpg
    caption:
  - project_image: /uploads/BevLibrary011.jpg
    caption:
  - project_image: /uploads/BevLibrary032.jpg
    caption:
  - project_image: /uploads/BevLibrary037.jpg
    caption:
_array_structures:
  award: 
    value:
      url:
      title:
award:
---


To meet the City’s commitment to improving the environmental quality of life for all Chicagoans, sustainable-design building strategies were fully integrated into the construction process, including technologies that reduce energy consumption, conserve water, and increase use of recycled content and locally manufactured materials. Due to these efforts, this facility easily achieved LEED Silver Status.

*“The Library and the Public Building Commission project teams, which met regularly with the Blinderman team, reported that the project ran particularly smoothly thanks to the cooperative attitude and professionalism of the Blinderman personnel. Of high importance to the Library of course, was the integrity and creative problem solving which Blinderman brought to this project.”*

**Mary A. Dempsey**
<br>Commissioner
<br>City of Chicago
<br>Chicago Public Library