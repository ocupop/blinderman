---
layout: project
title: United Airlines Hangars 770 and 710
subtitle: "Working with United Airlines airside at O'hare International Airport."
intro: "This project presented unorthodox challenges. Maintaining the functionality of these busy hangar facilities 24/7 was mission-critical. High working conditions (90 feet above the finished floor), and seamless coordination and communication with the United Airlines operations and maintenance team was critical to our team's success."
location: Chicago o’hare international airport
expertise_area: transportation
client: United airlines
value: 10.9M
scope: Complex Renovation
delivery: Firm Fixed Price
size: '55,000 SF (Hangar 770) / 257,600 SF (Hangar 710)'
video_thumbnail:
video_url:
project_images:
  - project_image: /uploads/united-hanger-03-03-17-001.jpg
    caption:
  - project_image: /uploads/united-hanger-03-03-17-006.jpg
    caption:
  - project_image: /uploads/united-hanger-03-03-17-016.jpg
    caption:
  - project_image: /uploads/united-hanger-03-03-17-020.jpg
    caption:
  - project_image: /uploads/united-hanger-03-03-17-024.jpg
    caption:
  - project_image: /uploads/united-hanger-03-03-17-029.jpg
    caption:
  - project_image: /uploads/united-hanger-03-03-17-030.jpg
    caption:
  - project_image: /uploads/united-hanger-03-03-17-035.jpg
    caption:
  - project_image: /uploads/united-hanger-03-03-17-041.jpg
    caption:
_array_structures:
  award: 
    value:
      url:
      title:
award:
---


This heat improvement and modernization project for United Airlines replaced two boilers and associated steam piping and forced air unit heaters in Hangar 770. All existing steam and gas fed unit heaters plus air handling units were removed and replaced in Hanger 710, where the mechanical renovation also included new electrical supply and controls for heating systems, and structural steel modifications to existing catwalks.

Proactive planning and strategic project management decisions allowed the Blinderman team to overcome delays in permit issuance and equipment approvals, and avoid conflicts with day-to-day United Hangar operations.