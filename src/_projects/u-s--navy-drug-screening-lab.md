---
layout: project
title: U.S. Navy Drug Screening Lab
subtitle: 'Designed with a layout and high-tech building systems that better support the drug screening processes. The structure not only accommodates the sensitive, specialized laboratory equipment, it conforms to energy efficiency and certification requirements.'
intro: This facility supports the Navy’s mission to ensure warfighter readiness by deterring illegal drug use through accurate forensic drug testing. The previous base laboratory resided in an inefficient World War II era building that originally housed the base obstetric  ward.
expertise_area: government
location: 'Naval Station Great Lakes, IL'
client: U.S. Navy
value: 15M
scope: New Construction
delivery: Design-Bid-Build
size: '2-stories, 28,000 SF'
video_thumbnail:
video_url:
project_images:
  - project_image: /uploads/druglab-GLNB_05.jpg
    caption:
  - project_image: /uploads/Drug-LabGNLB_13.jpg
    caption:
  - project_image: /uploads/druglab-GLNB_06.jpg
    caption:
  - project_image: /uploads/druglab-GLNB_07.jpg
    caption:
  - project_image: /uploads/DrugLab-GLNB_09.jpg
    caption:
  - project_image: /uploads/druglab-GLNB_10.jpg
    caption:
  - project_image: /uploads/DrugLab-GLNB_11.jpg
    caption:
  - project_image: /uploads/DrugLab-GLNB_12.jpg
    caption:
  - project_image: /uploads/DrugLab_SteelTopping_Pic3.jpg
    caption:
_array_structures:
  award: 
    value:
      url:
      title:
award:
---


The new, 2-story building is designed with a layout and high-tech building systems that better support the drug screening processes. The structure not only accommodates the sensitive, specialized laboratory equipment, it conforms to energy efficiency and certification requirements under the U.S. Green Building Council’s Leadership in Energy and Environmental Design program, and is LEED Silver Certified.

Drug testing functions – which include the accessioning, automated immunoassay screening, quality control, confirmation laboratories, plus all drug testing documentation support spaces – are located on the first floor. The second floor supports the laboratory’s administrative functions.

Blinderman earned a STAR Safety Award from NAVFAC for exceptional safety performance and commitment to safety on the construction of the Navy Drug Screening Laboratory. The award recognizes the successful execution of more than 63,000 hours worked on the construction of this new facility with zero recordable incidents and non-compliance notices.