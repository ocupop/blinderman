---
layout: project
title: 99th Street Metra Train Station
subtitle: >-
  Built in the early 1890’s, the 99th Street METRA station is a designated
  landmark in the 19th Ward’s Historic Train Station District.
intro: >-
  As a designated landmark in the 19th Ward’s Historic Train Station District,
  this neglected station required an intricate, true-to-original renovation,
  adhering to the City of Chicago’s rigorous landmark preservation standards.
location: 'Chicago, IL'
expertise_area: transportation
client: Metra
value: 2.2M
scope: Historic Renovation
delivery: Design-Bid-Build
size:
video_thumbnail:
video_url:
project_images:
  - project_image: /uploads/99th_metra_05.jpg
    caption:
  - project_image: /uploads/99th_metra_03.jpg
    caption:
  - project_image: /uploads/99th_metra_06.jpg
    caption:
  - project_image: /uploads/99th_metra_09.jpg
    caption:
  - project_image: /uploads/99thStreet-in-prog.jpg
    caption:
_array_structures:
  award: 
    value:
      url:
      title:
award:
---

Over the years, the station house interior suffered from extensive water damage and general age deterioration. After being completely gutted, the entire structure was raised, and the original wooden timber foundation was replaced with a concrete pad.

Other major structural modifications included removing the second floor to create a 2-story foyer. Significant care was taken in expertly refurbishing significant architectural elements such as windows, ornate corbel brackets, and the distinctive onion-dome cupola.

Construction of a new, heated platform shelter that was designed to match the renovated station house added to commuter comfort. Commuter convenience was further enhanced with the installation of an ADA compliant platform, additional parking area and site utility improvements.