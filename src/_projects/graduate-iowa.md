---
layout: project
title: Graduate Iowa
subtitle: Former Sheraton hotel graduates with style in Iowa.
intro: >-
  The Blinderman team embodied hospitality know-how on this, the third of six
  complex renovation projects, performed for AJ Capital Partners to give this
  former Sheraton hotel a nine-story, 231-room facelift and meet the demands of
  an established gradual opening schedule.
location: 'Iowa City, IA'
expertise_area: hospitality
client: AJ Capital Partners
value:
scope: Complex Renovation
delivery: CM at Risk
size: '9-stories, 231 keys'
video_thumbnail:
video_url:
project_images:
  - project_image: /uploads/graduate-iowa-lobby.jpg
    caption:
  - project_image: /uploads/graduate-iowa-front.jpg
    caption:
  - project_image: /uploads/graduate-iowa-ballroom.jpg
    caption:
  - project_image: /uploads/graduate-iowa-pool.jpg
    caption:
_array_structures:
  award: 
    value:
      url:
      title:
award:
---

Our journey with the Graduate Iowa Hotel began with an extensive preconstruction phase. Blinderman worked closely with the owner, architect and subcontractors to develop comprehensive and accurate construction budgets. Value Engineered solutions were identified, vetted and implemented to meet the owner's project budget. Our team built-out a model room to ensure all FF&E met the exacting standards of the Graduate brand.

The scope of work included updated finishes throughout the guestrooms, corridors, and 9,000 SF of event space, including a ballroom, multiple breakout rooms and a boardroom.

The hotel reconfiguration created exciting new amenity spaces including a Topgolf Swing Suite, the Poindexter Coffee cafe, and Gene’s, a 1970’s-inspired bar and restaurant (in homage to the late Gene Wilder, a University of Iowa alumnus). Beyond the practical installation of a new roof, the tired building exterior was enlivened with a plaid super graphic, refreshing the Iowa City skyline with bold Hawkeye color.

Blinderman oversaw and executed the hand-crafted details tying the Graduate brand in a unique way to the local experience. Inspiration for the decor was drawn from the prestigious Iowa Writers’ Workshop and Iowa City’s distinction as a UNESCO City of Literature. Our crews hung wall panels covered with handwritten literary quotes from local authors on the lobby walls. The farm-themed wall paper installed in the hallways features pigs, cows, corn and Crunch Berries cereal, nods to both agronomy in the state and the Quaker Oats plant in Cedar Rapids.

AJ Capital relies on Blinderman’s ability to manage and self-perform complicated custom design details, guaranteeing the quality and timeliness required for a true Graduate experience. &nbsp;