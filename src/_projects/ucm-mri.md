---
layout: project
title: MRI and CT Scan Relocation
subtitle: Renovating within operational spaces to meet all the technical nuances and intricacies of very sensitive medical imaging equipment.
intro: "Blinderman’s team met the challenges of this complex project by working odd and fragmented shifts, maintaining the level of cleanliness crucial in an operational patient care facility, and performing disruptive tasks within extremely tight time tolerances \nallowed by the hospital schedule."
location: 'Chicago, IL'
expertise_area: healthcare
client: University of Chicago Medical Center
value: 2.2M
scope: Complex Occupied Renovation
delivery: Design-Bid-Build
size: '20,000 SF'
video_thumbnail:
video_url:
project_images:
  - project_image: /uploads/uofchg-mri001.jpg
    caption:
  - project_image: /uploads/uofchg-mri017.jpg
    caption:
  - project_image: /uploads/uofchg-mri005.jpg
    caption:
  - project_image: /uploads/uofchg-mri008.jpg
    caption:
  - project_image: /uploads/uofchg-mri009.jpg
    caption:
  - project_image: /uploads/uofchg-mri013.jpg
    caption:
  - project_image: /uploads/uofchg-mri020.jpg
    caption:
  - project_image: /uploads/uofchg-mri021.jpg
    caption:
  - project_image: /uploads/uofchg-mri024.jpg
    caption:
  - project_image: /uploads/uofchg-mri025.jpg
    caption:
_array_structures:
  award: 
    value:
      url:
      title:
award:
---


When flooding unrelated to the construction occurred, spilling thousands of gallons of water into the outpatient facility, our team reacted quickly – above and beyond the scope of contract – to immediately affect the emergency repairs necessary to avert catastrophe and mitigate damage.

Our expertise in complex renovation was put to play once again when our team engineered, *in situ*, an isolated frame, densely insulated partition system, successfully minimizing the transfer of substantial noise from the MRI equipment into corridors and patient care areas.

To keep building harmonic vibrations and electronic interference from affecting the quality of MRI images, the specifications required isolating the entire building from the vibration of the significantly more powerful 3T MRI machine. The new MRI machine rests on an independent floor slab that is supported from beneath on a steel structure and concrete piers that are buried four feet into the earth.

The resulting isolation was so successful, the structure did not even register on the scale during testing. The MRI images produced on this piece of equipment are so sharp and clear, the technicians claim the quality is comparable to high definition television.