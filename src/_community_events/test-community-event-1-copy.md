---
layout: community-event
title: >-
  2018 Lake Michigan Post of The Society of American Military Engineers Golf
  Outing
image: /uploads/same-2018-3.jpg
cause_description: >-
  The Society of American Military Engineers (SAME) is the only nonprofit
  professional engineering education organization that actively promotes the
  advancement of both individual technical knowledge and the collective
  engineering capabilities of governments, the uniformed services and private
  industry.
event_url:
staff:
  - steven-blinderman
  - dave-byrd
  - adam-kucharski
  - conor-malaney
  - ed-gromala
---

The Lake Michigan Post of The Society of American Military Engineers (SAME) hosted their annual golf outing on June 15th at the Midlane Golf Resort in Wadsworth, IL, to raise funds for their college scholarship fund. This year's SAME event raised $16,000 for scholarships.

SAME Scholarships of $1,000 to $3,000 are awarded to graduating high school students or full-time college students at an accredited college or university during the 2018-2019 school year who are pursuing degree in engineering, architecture, or an environmental or applied science (i.e. mathematics, physics, chemistry, geology, etc.) with the ultimate goal of pursuing a career in construction, engineering, architecture, environmental science, or a physical science associated with these career fields. The major emphasis for this scholarship is on the construction of physical elements of community life such as buildings, highways, utility and mechanical systems, and those technologies which advance the community’s well being.