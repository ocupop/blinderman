---
layout: community-event
title: Help Thousand Waves Meet its Spirit Challenge Fundraising Goal
image: /uploads/thousandwaves2.jpg
cause_description: "Thousand Waves\_Martial Arts & Self-Defense Center is a not-for-profit in Chicago dedicated to empowerment, peace, and healing."
event_url:
staff:
  - ian-pantale
  - emma-ghariani
  - dave-byrd
  - ryan-cummings
  - ed-gromala
---

[Thousand Waves Martial Arts & Self-Defense Center](http://thousandwaves.org/) is a not-for-profit in Chicago dedicated to empowerment, peace, and healing. Over the past 33 years, it has strived for diversity and equal access to its services including karate and self-defense classes, and community programs. Now, it continues to be sustained by volunteer efforts that enable the expansion of anti-violence and empowerment programs reaching more adults and children both within the school and in the greater Chicago area.

Our Project Coordinator, Ian Pantale, has been working hard to assist with their Spirit Challenge Fundraiser, which is so close to its goal that they extended the May 5 donation deadline. The fundraiser supports the following three areas:

* A continued scholarship program, which provides funding for children and adults who wouldn’t otherwise be able to study karate or self-defense
* Adapted Seido Karate program (ASK), which provides karate for youth with disabilities
* Self-Defense/Violence Prevention program, which provides Self-defense and Bystander Intervention workshops and courses all over the city

Click [here](https://thousandwaves.networkforgood.com/projects/50054-ian-pantale-s-spirit-challenge-fundraiser){: target="_blank"} to help support Ian’s fundraising initiative for Thousand Waves.