---
layout: community-event
title: Volunteer to Feed the Homeless at the Open Center for the Arts
image: /uploads/feedthehomeless3.jpg
cause_description: >-
  The mission of OPEN Center for the Arts is to provide a space where all
  artists can come together to educate, showcase, refine, and develop their
  talents as well as support entrepreneurship opportunities in the arts while
  connecting their growth to the community.
event_url:
staff:
  - janet-rodriguez
  - margo-olson
  - kareem-abes
  - steven-colberg
  - thomas-white
  - steven-blinderman
  - david-blinderman
  - brittany-long
  - ian-pantale
  - christine-blevins
  - ryan-cummings
  - brian-gundlach
  - dave-byrd
---

The [Open Center for the Arts'](http://www.opencenterforthearts.org/){: target="_blank"} is hosting its 4th annual "Feed the Homeless" event on January 27, 2017 from 9 am - 12 pm. <br><br>Last year, they distributed 280 hot meals and care packages throughout the city of Chicago. This was made possible by all the individuals that donated funds, items and volunteered to assemble hot meals and care packages and distribute them to homeless individuals.<br><br>This year's goal this year is to personally deliver 500 hot meals and care packages to homeless individuals. Here's how can you support this event: 

1. By donating funds to support the event: [https://squareup.com/store/opencenterforthearts](https://squareup.com/store/opencenterforthearts){: target="_blank"}
2. By donating items on the list. You can find the list on the invite image. 
3. By volunteering on Saturday Morning from 9 am -12 pm putting together the hot meals and care packages and/or distributing them throughout the city,